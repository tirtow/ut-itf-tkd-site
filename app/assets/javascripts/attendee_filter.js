$(document).on("turbolinks:load", function () {
  $("#attendee-search").keyup(function (e) {
    filter_attendees("");
  });
  $(".attendee-filter-select").change(function (e) {
    filter_attendees("");
  });
});

/**
 * Filters attendees based on the selects and the text box
 */
function filter_attendees(sort, dir) {
    $.ajax("/tournaments/" + $("#tournament-path").text() +
           "/attendees/filter?" +
           "search=" + $("#attendee-search").val() +
           "&rank=" + $("#rank-select").val() +
           "&weight_class=" + $("#weight-class-select").val() +
           "&event=" + $("#event-select").val());
}
