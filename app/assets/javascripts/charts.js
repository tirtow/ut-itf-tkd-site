$(document).on("turbolinks:load", function() {
  // The different kinds of charts
  let charts = ["column", "pie", "line", "area", "percentarea"]

  // Toggles the shown chart for charts grouped by different types
  $("[id$=-chart-toggle]").change(function() {
    // Get the name prefix for the chart's ID
    var name = $(this).attr("id").replace("-chart-toggle", "")
    let end = name.lastIndexOf("-")
    name = name.slice(0, end + 1)

    jQuery.each(charts, function(index, chart) {
      $("#" + name + chart).hide()
    })

    $("#" + $(this).attr("id").replace("-chart-toggle", "")).show()
  })
 
  // Toggles the shown chart for charts grouped by year
  $("[id*=-category-group-]").change(function() {
    // Get the name prefix for the chart's ID
    var name = $(this).attr("id").replace("-toggle", "")
    let end = name.lastIndexOf("-")
    name = name.slice(0, end + 1)

    // As long as there is a chart element, hide it. Do not check for whether
    // 2013 exists since data is inconsistent.
    var year = 2013
    while (year == 2013 || $("#" + name + year + "-pie").length) {
      $("#" + name + year + "-pie").hide()
      year++
    }

    // Show the selected chart
    $("#" + $(this).attr("id").replace("-toggle", "-pie")).show()
  })
})
