$(document).on("turbolinks:load", function () {
  $("#checkin-search").keyup(function (e) {
    $.ajax("/tournaments/" + $("#tournament-path").text() + "/checkin/filter?" +
           "query=" + $("#checkin-search").val());
  });
});
