class AboutTextsController < ApplicationController

  before_action :logged_in_user
  before_action :valid_about_text, only: [:edit, :update]


  ##
  # Index page of all text
  # GET /texts
  def index
    @about_texts = AboutText.order(section: :asc)
  end

  ##
  # Form for a user to edit an about text
  # GET /texts/:section/edit
  def edit
    @about_text = AboutText.get(params[:section])
  end

  ##
  # Updates the about text
  # PATCH /texts/:section/edit
  def update
    @about_text = AboutText.get(params[:section])
    if @about_text.update_attributes(update_params)
      flash[:success] = "Successfully updated text"
      redirect_to(root_url)
    else
      render("edit")
    end
  end

  private

  ##
  # The params to update an about text
  def update_params
    params.require(:about_text).permit(:text)
  end

  ##
  # Ensures a valid about text is being accessed
  def valid_about_text
    if AboutText.get(params[:section]).nil?
      flash[:danger] = "Could not find that about text"
      redirect_to(root_url)
    end
  end
end
