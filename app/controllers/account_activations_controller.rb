class AccountActivationsController < ApplicationController

  before_action :not_logged_in

  ##
  # Handles user activating an account
  # Checks that the account is valid to be activated, if so activates the
  # account and logs the user in
  def edit
    user = User.find_by(email: params[:email])

    if user.nil?
      flash[:danger] = "No account found with the given email"
      redirect_to(root_url)
    elsif user.activated?
      flash[:warning] = "Account has already been activated"
      redirect_to(root_url)
    elsif user.authenticated?(:activation, params[:id])
      user.activate
      login(user)
      redirect_to(profile_password_set_url)
    else
      flash[:danger] = "Invalid activation link"
      redirect_to(root_url)
    end
  end
end
