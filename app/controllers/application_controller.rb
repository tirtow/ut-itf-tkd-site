class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  rescue_from StandardError, with: :internal_error

  include SessionsHelper

  before_action :has_password

  private

  ##
  # Redirect if no user is logged in
  def logged_in_user
    if !logged_in?
      redirect_to(root_url)
    end
  end

  ##
  # Redirect if user has not set password
  def has_password
    if logged_in? && current_user.password_digest.nil?
      flash[:warning] = "You need to set a password"
      redirect_to(profile_password_set_url)
    end
  end

  ##
  # Redirect user if they are logged in
  def not_logged_in
    if logged_in?
      redirect_to(root_url)
    end
  end

  ##
  # Redirect away if invalid tournament ID given
  def valid_tournament
    tournament = Tournament.get(params[:id])
    if tournament.nil?
      flash[:danger] = "Could not find that tournament"
      redirect_to(tournaments_path)
    end
  end

  ##
  # Send slack message about error and then raise again
  # exception:  the error that was received
  def internal_error(exception)
    begin
      if Rails.env.production?
        notifier = Slack::Notifier.new(Figaro.env.slack_webhook,
                                       channel: "server-errors")
        notifier.ping("*Internal error:*\n```#{exception.message}```")
      end
    rescue
    end

    raise
  end
end
