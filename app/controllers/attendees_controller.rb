class AttendeesController < ApplicationController

  before_action :logged_in_user,    only: [:show, :checkin, :uncheckin, :paid,
                                           :unpaid]
  before_action :valid_tournament
  before_action :valid_attendee,    only: [:show, :checkin, :uncheckin, :paid,
                                           :unpaid]
  before_action :registration_open, only: [:new, :create]

  ##
  # Form to register for a tournament
  # GET /tournaments/:id/register
  def new
    @tournament = Tournament.get(params[:id])
    @attendee = Attendee.new
  end

  ##
  # Register the user for the tournament
  # POST /tournaments/:id/register
  def create
    @tournament = Tournament.get(params[:id])
    @attendee = Attendee.new(create_params)

    if verify_recaptcha(model: @attendee) && @attendee.save
      @attendee.send_confirmation_email
      redirect_to(attendees_confirmation_path)
    else
      render("new")
    end
  end

  ##
  # Show confirmation to user after registering
  # GET /tournaments/:id/register/confirmation
  def confirm
    @tournament = Tournament.get(params[:id])
  end

  ##
  # Shows information for a single attendee
  # GET /tournaments/:id/attendees/:attendee_id
  def show
    @tournament = Tournament.get(params[:id])
    @attendee = Attendee.find(params[:attendee_id])
  end

  ##
  # Checks in an attendee
  # PATCH /tournaments/:id/attendees/:attendee_id/checkin
  def checkin
    tournament = Tournament.get(params[:id])
    attendee = Attendee.find(params[:attendee_id])
    if !attendee.update_attribute(:checked_in, true)
      flash[:danger] = "Failed to check in #{attendee.first_name} " +
                       "#{attendee.last_name}"
    end

    redirect_to(tournaments_checkin_path(id: tournament.path,
                                         query: params[:query]))
  end

  ##
  # Removes check in from an attendee
  # PATCH /tournaments/:id/attendees/:attendee_id/uncheckin
  def uncheckin
    tournament = Tournament.get(params[:id])
    attendee = Attendee.find(params[:attendee_id])
    if !attendee.update_attribute(:checked_in, false)
      flash[:danger] = "Failed to remove check in from " +
                       "#{attendee.first_name} #{attendee.last_name}"
    end

    redirect_to(tournaments_checkin_path(id: tournament.path,
                                         query: params[:query]))
  end

  ##
  # Marks an attendee as having had paid their entry fee
  # PATCH /tournaments/:id/attendees/paid
  def paid
    tournament = Tournament.get(params[:id])
    attendee = Attendee.find(params[:attendee_id])
    if !attendee.update_attribute(:paid, true)
      flash[:danger] = "Failed to mark #{attendee.first_name} " +
                      "#{attendee.last_name} as paid."
    end

    redirect_to(tournaments_checkin_path(id: tournament.path,
                                         query: params[:query]))
  end

  ##
  # Removes mark as paid from an attendee
  # PATCH /tournaments/:id/attendees/unpaid
  def unpaid
    tournament = Tournament.get(params[:id])
    attendee = Attendee.find(params[:attendee_id])
    if !attendee.update_attribute(:paid, false)
      flash[:danger] = "Failed to remove mark as paid from " +
                       "#{attendee.first_name} #{attendee.last_name}"
    end

    redirect_to(tournaments_checkin_path(id: tournament.path,
                                         query: params[:query]))
  end

  private

  ##
  # Parameters to create an attendee
  def create_params
    params.require(:attendee).permit(:first_name, :last_name, :rank, :school,
                                     :instructor, :patterns, :sparring,
                                     :breaking, :age, :weight_class, :sex,
                                     :street, :city, :zip, :phone, :email,
                                     :team_demo, :team_demo_name,
                                     :accepted_waiver, :tournament_id, :state)
  end

  ##
  # Redirect user if registration is not open for the tournament
  def registration_open
    tournament = Tournament.get(params[:id])
    if !tournament.registration_open?
      flash[:danger] = "Registration is not open yet for #{tournament.name}"
      redirect_to(tournaments_path)
    end
  end

  ##
  # Redirect user if given invalid attendee ID
  def valid_attendee
    tournament = Tournament.get(params[:id])
    attendee = Attendee.find_by(id: params[:attendee_id])
    if attendee.nil?
      flash[:danger] = "Failed to find that attendee"
      redirect_to(tournament_path(id: tournament.path))
    end
  end
end
