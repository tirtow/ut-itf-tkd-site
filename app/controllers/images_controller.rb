class ImagesController < ApplicationController

  before_action :logged_in_user
  before_action :valid_image,    only: [:edit, :update]

  ##
  # Shows all images
  # GET /images
  def index
    @images = Image.order(name: :asc)
  end

  ##
  # Page to change the image
  # GET /images/:id/edit
  def edit
    @image = Image.get(params[:id])
  end

  ##
  # Updates an image
  # PATCH /images/:id/edit
  def update
    @image = Image.get(params[:id])
    if @image.update_attributes(update_params)
      flash[:success] = "Successfully updated #{@image.name}"
      redirect_to(images_path)
    else
      render("edit")
    end
  end

  private

  ##
  # Parameters for updating an image
  def update_params
    params.require(:image).permit(:photo)
  end

  ##
  # Ensures a valid image is being accessed
  def valid_image
    if Image.get(params[:id]).nil?
      flash[:danger] = "Could not find that image"
      redirect_to(images_path)
    end
  end
end
