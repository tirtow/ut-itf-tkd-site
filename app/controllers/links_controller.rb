class LinksController < ApplicationController

  before_action :logged_in_user, only: [:index, :edit, :update]
  before_action :valid_link,     only: [:edit, :update]

  ##
  # Redirects to the 40for40 donation page
  # GET /donate
  def donate
    redirect_to_link("donate", "donate")
  end

  ##
  # Redirects to store page for non-traveling dues
  # GET /dues/non-traveling
  def dues_regular
    redirect_to_link("dues_regular", "non-traveling dues")
  end
 
  ##
  # Redirects to store page for traveling dues
  # GET /dues/traveling
  def dues_traveling
    redirect_to_link("dues_traveling", "traveling dues")
  end

  ##
  # Redirects to the volunteer signup page
  # GET /volunteer
  def volunteer
    redirect_to_link("volunteer", "volunteer")
  end

  ##
  # Redirects to the membership packet page
  # GET /join
  def join
    redirect_to_link("join", "membership packet")
  end

  ##
  # Display all links
  # GET /links
  def index
    @links = Link.order(name: :asc)
  end

  ##
  # Form to edit a link
  # GET /links/:name/edit
  def edit
    @link = Link.get(params[:name])
  end

  ##
  # Updates the url for a link
  # PATCH /links/:name/edit
  def update
    @link = Link.get(params[:name])
    if @link.update_attributes(update_params)
      flash[:success] = "Successfully updated #{@link.name}"
      redirect_to(links_path)
    else
      render("edit")
    end
  end

  private

  ##
  # Parameters to update a link
  def update_params
    params.require(:link).permit(:url)
  end

  ##
  # Ensure that a valid link is given
  def valid_link
    link = Link.get(params[:name])
    if link.nil?
      flash[:danger] = "Invalid link name #{params[:name]}"
      redirect_to(links_path)
    end
  end

  ##
  # Redirects to the link with the given name or to root_path if cannot find
  # name:         the name of the link
  # pretty_name:  the name to display if cannot find link
  def redirect_to_link(name, pretty_name)
    link = Link.get(name)
    if !link.nil?
      redirect_to(link.url)
    else
      flash[:danger] = "Could not find #{pretty_name} url."
      redirect_to(root_path)
    end
  end

end
