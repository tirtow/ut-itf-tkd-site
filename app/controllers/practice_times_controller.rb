class PracticeTimesController < ApplicationController

  before_action :logged_in_user
  before_action :valid_practice_time, only: [:destroy]

  ##
  # Form to create a new practice time
  # GET /practices/new
  def new
    @practice_time = PracticeTime.new
  end

  ##
  # Creates a new practice time
  # POST /practices/new
  def create
    @practice_time = PracticeTime.new(create_params)
    if @practice_time.save
      flash[:success] = "Created new practice time"
      redirect_to(practices_path)
    else
      render("new")
    end
  end

  ##
  # Lists all practice times
  # GET /practices
  def index
    @practice_times = PracticeTime.order(day: :asc)
  end

  ##
  # Deletes a practice time
  # DELETE /practices/:id
  def destroy
    PracticeTime.find(params[:id]).delete
    flash[:success] = "Successfully deleted practice time"
    redirect_to(practices_path)
  end

  private

  ##
  # Parameters to create a practice time
  def create_params
    params.require(:practice_time).permit(:day, :time, :location)
  end

  ##
  # Ensures a valid practice time is given
  def valid_practice_time
    if PracticeTime.find_by(id: params[:id]).nil?
      flash[:danger] = "Could not find that practice time"
      redirect_to(practices_path)
    end
  end
end
