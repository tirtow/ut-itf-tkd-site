class RepresentativesController < ApplicationController

  before_action :logged_in_user

  ##
  # Form for a user to create a new representative
  # GET /representatives/new
  def new
    @rep = Representative.new
  end

  ##
  # Creates the new representative
  # POST /representatives/new
  def create
    @rep = Representative.new(create_params)
    if @rep.save
      flash[:success] = "Created #{@rep.name}"
      redirect_to(representatives_path)
    else
      if !Rails.env.test?
        @rep.remove_photo!
      end
      render("new")
    end
  end

  ##
  # Deletes a representative
  # DELETE /representatives/:id
  def destroy
    rep = Representative.find_by(id: params[:id])
    if !rep.nil?
      name = rep.name
      rep.destroy
      flash[:success] = "Deleted #{name}."
    end

    redirect_to(representatives_path)
  end

  ##
  # Page to edit a representative's information
  # GET /representatives/:id/edit
  def edit
    @rep = Representative.find_by(id: params[:id])
  end

  ##
  # Updates a representative's information
  # PATCH /representatives/:id/edit
  def update
    @rep = Representative.find_by(id: params[:id])
    if @rep.update_attributes(update_params)
      flash[:success] = "Successfully updated #{@rep.name}"
      redirect_to(representatives_path)
    else
      render("edit")
    end
  end

  ##
  # Gets all the representatives
  # GET /representatives
  def index
    @reps = Representative.order(position: :asc, name: :asc)
  end

  private

  ##
  # Parameters for creating a representative
  def create_params
    params.require(:representative).permit(:name, :email, :position, :photo)
  end
 
  ##
  # Parameters for updating a representative
  def update_params
    params.require(:representative).permit(:name, :email, :position)
  end

end
