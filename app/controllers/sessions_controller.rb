class SessionsController < ApplicationController

  before_action :not_logged_in, only: [:new, :create]

  ##
  # Form for a user to login
  # GET /login
  def new
  end

  ##
  # Logs in the user
  # POST /login
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.activated? && user.authenticate(params[:session][:password])
      login(user)
      redirect_to(root_url)
    elsif user && !user.activated?
      flash.now[:warning] = "Check your email to activate your account."
      render("new")
    else
      flash.now[:danger] = "Invalid email/password combination"
      render("new")
    end
  end

  ##
  # Logs out the user
  # DELETE /logout
  def destroy
    if logged_in?
      logout
    end

    redirect_to(root_url)
  end

end
