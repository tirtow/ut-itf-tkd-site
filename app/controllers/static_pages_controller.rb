class StaticPagesController < ApplicationController

  ##
  # The home page for the site
  # GET /
  def home
    @no_container = true
    @reps = Representative.order(position: :asc, name: :asc)
    @about_texts = AboutText.get_hash
    @practice_times = PracticeTime.order(day: :asc)
    @images = Image.get_hash
    @sheets = TestingSheet.order(order: :asc)
  end

  ##
  # Helper page to route users to the correct dues pages
  # GET /dues
  def dues
    @dues = Link.get_hash
  end
end
