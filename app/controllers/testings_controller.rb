class TestingsController < ApplicationController

  before_action :logged_in_user, only: [:index, :edit, :update]
  before_action :valid_sheet,    only: [:edit, :update]

  ##
  # Redirects to the home page to the testing section
  # GET /testing
  def testing
    redirect_to(root_path(anchor: "testing"))
  end

  ##
  # Shows all testing sheets for admins
  # GET /testing/sheets/edit
  def index
    @sheets = TestingSheet.order(order: :asc)
  end

  ##
  # Form to change a testing sheet
  # GET /testing/sheets/:id/edit
  def edit
    @sheet = TestingSheet.get(params[:id])
  end

  ##
  # Updates a testing sheet
  # PATCH /testing/sheets/:id/edit
  def update
    @sheet = TestingSheet.get(params[:id])
    if @sheet.update_attributes(update_params)
      flash[:success] = "Successfully uploaded a new file for #{@sheet.title}"
      redirect_to(testing_sheets_edit_path)
    else
      render("edit")
    end
  end

  private

  ##
  # Parameters for updating a testing sheet
  def update_params
    params.require(:testing_sheet).permit(:file)
  end

  ##
  # Ensures a valid testing sheet is being accessed
  def valid_sheet
    if TestingSheet.get(params[:id]).nil?
      flash[:danger] = "Invalid testing sheet #{params[:id]}"
      redirect_to(testing_sheets_edit_path)
    end
  end
end
