class TournamentsController < ApplicationController

  before_action :logged_in_user,   only: [:new, :create, :manage,
                                          :open_registration,
                                          :close_registration,
                                          :checkin, :edit, :update,
                                          :generate_csv, :filter_checkin,
                                          :show_attendees, :filter_attendees]
  before_action :valid_tournament, only: [:show, :checkin, :manage,
                                          :open_registration,
                                          :close_registration,
                                          :edit, :update, :generate_csv,
                                          :filter_checkin, :show_attendees,
                                          :filter_attendees]

  ##
  # Form for user to create new tournament
  # GET /tournaments/new
  def new
    @tournament = Tournament.new
  end

  ##
  # Creates the new tournament
  # POST /tournaments/new
  def create
    @tournament = Tournament.new(create_params)
    if @tournament.save
      flash[:success] = "Created tournament #{@tournament.name}"
      redirect_to(tournaments_path)
    else
      render("new")
    end
  end

  ##
  # Shows all tournaments
  # GET /tournaments
  def index
    @tournaments = Tournament.order(event_date: :desc)
  end

  ##
  # Shows information for the given tournament
  # GET /tournaments/:id
  def show
    @tournament = Tournament.get(params[:id])
  end

  ##
  # Management options for the given tournament
  # GET /tournaments/:id/manage
  def manage
    @tournament = Tournament.get(params[:id])
    @attendees = @tournament.filter_attendees
  end

  ##
  # Opens registration for the given tournament
  # PATCH /tournaments/:id/open
  def open_registration
    tournament = Tournament.get(params[:id])
    if tournament.update_attribute(:registration_open, true)
      flash[:success] = "Opened registration for #{tournament.name}"
    else
      flash[:danger] = "Failed to open registration for #{tournament.name}"
    end
    redirect_to(tournaments_manage_path(id: tournament.path))
  end
 
  ##
  # Closes registration for the given tournament
  # PATCH /tournaments/:id/close
  def close_registration
    tournament = Tournament.get(params[:id])
    if tournament.update_attribute(:registration_open, false)
      flash[:success] = "Closed registration for #{tournament.name}"
    else
      flash[:danger] = "Failed to close registration for #{tournament.name}"
    end
    redirect_to(tournaments_manage_path(id: tournament.path))
  end

  ##
  # Form to check in attendees
  # GET /tournaments/:id/checkin
  def checkin
    @query = params[:query] || ""
    @tournament = Tournament.get(params[:id])
    @attendees = @tournament.filter_attendees(@query)
    @no_container = true
  end

  ##
  # Handles ajax request to filter checkin as user types
  # GET /tournaments/:id/checkin/filter
  def filter_checkin
    @query = params[:query] || ""
    @tournament = Tournament.get(params[:id])
    @attendees = @tournament.filter_attendees(@query)
    @no_container = true

    respond_to do |format|
      format.js
    end
  end

  ##
  # Form to edit a tournament
  # GET /tournaments/:id/edit
  def edit
    @tournament = Tournament.get(params[:id])
  end

  ##
  # Updates a tournament
  # PATCH /tournaments/:id/edit
  def update
    @tournament = Tournament.get(params[:id])
    if @tournament.update_attributes(create_params)
      flash[:success] = "Successfully updated #{@tournament.name}"
      redirect_to(tournaments_manage_path(id: @tournament.path))
    else
      render("edit")
    end
  end

  ##
  # Generates a CSV of the attendees for a tournament
  # GET /touranments/:id/csv
  def generate_csv
    tournament = Tournament.get(params[:id])
    send_data(tournament.to_csv, filename: tournament.csv_name)
  end

  def show_attendees
    @tournament = Tournament.get(params[:id])
    @attendees = @tournament.attendees.order(rank: :asc,
                                             last_name: :asc,
                                             first_name: :asc)
    @no_container = true
    @search = params[:search] || ""
    @rank = params[:rank] || ""
    @weight_class = params[:weight_class] || ""
    @event = params[:event] || ""
    @sort = params[:sort] || ""
    @dir = params[:dir] || "asc"
    @tournament = Tournament.get(params[:id])
  end

  def filter_attendees
    @search = params[:search] || ""
    @rank = params[:rank] || ""
    @weight_class = params[:weight_class] || ""
    @event = params[:event] || ""
    @sort = params[:sort] || ""
    @dir = params[:dir] || ""
    @tournament = Tournament.get(params[:id])
    @attendees = @tournament.filter_attendees(@search, @rank, @weight_class,
                                              @event).to_a
    if !@sort.blank?
      @attendees.sort_by! do |attendee|
        val = attendee[@sort]
        if val.is_a?(String)
          val = val.downcase
        end

        val
      end
      if @dir == "desc"
        @attendees.reverse!
        @dir = "asc"
      else
        @dir = "desc"
      end
    end

    @no_container = true

    respond_to do |format|
      format.js
    end
  end

  private

  ##
  # Parameters for creating a tournament
  def create_params
    params.require(:tournament).permit(:name, :event_date, :path,
                                       :payment_link, :checkin_time,
                                       :start_time)
  end

end
