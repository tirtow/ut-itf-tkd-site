class TransactionsController < ApplicationController

  before_action :logged_in_user

  ##
  # The main page for transactions
  # GET /transactions
  def home
    @studentorg = Transaction.where(account: "studentorg")
    @allocations = Transaction.where(account: "allocation")
    @donations = Transaction.where(account: "donation")
  end

  ##
  # Form for the user to update transactions
  # GET /transactions/update
  def update
  end

  ##
  # Reads the transactions from the given file
  # POST /transactions/scrape
  def scrape
    page = Nokogiri::HTML(File.read(scrape_params[:import_file].path))
    Transaction.build_all(page)
    redirect_to(transactions_path)
  end

  ##
  # Shows the visualization charts for revenue
  # GET /charts/revenue
  def revenue_charts
    @revenue_by_year = RevenueChartData.revenue_by_year
    @revenue_by_semester = RevenueChartData.revenue_by_semester
    @revenue_by_category = RevenueChartData.revenue_by_category
    @revenue_by_category_and_year = RevenueChartData.revenue_by_category_and_year
    @revenue_by_category_splits = RevenueChartData.revenue_by_category_splits
    @dues_by_year = RevenueChartData.dues_by_year
    @dues_by_semester = RevenueChartData.dues_by_semester
    @tournament_by_year = RevenueChartData.tournament_by_year
    @donations_by_year = RevenueChartData.donations_by_year
    @donations_by_semester = RevenueChartData.donations_by_semester
    @allocations_by_year = RevenueChartData.allocations_by_year
    @misc_by_year = RevenueChartData.misc_by_year
  end

  ##
  # Shows the visualization charts for spending
  # GET /charts/spending
  def spending_charts
    @spending_by_year = SpendingChartData.spending_by_year
    @spending_by_semester = SpendingChartData.spending_by_semester
    @spending_by_category = SpendingChartData.spending_by_category
    @spending_by_category_and_year = SpendingChartData.spending_by_category_and_year
    @spending_by_category_splits = SpendingChartData.spending_by_category_splits
  end

  ##
  # Shows the visualization charts for comparison between revenue and spending
  def comparison_charts
    @comparison_by_year = ComparisonChartData.comparison_by_year
    @comparison_by_year_negated = ComparisonChartData.comparison_by_year(true)
    @comparison_by_totals = ComparisonChartData.comparison_by_totals
  end

  private

  ##
  # The parameters for scraping transactions
  def scrape_params
    params.require(:update).permit(:import_file)
  end
end
