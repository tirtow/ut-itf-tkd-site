class UsersController < ApplicationController

  before_action :logged_in_user
  before_action :valid_user,        only:   [:destroy]
  before_action :no_password,       only:   [:set_password, :create_password]

  skip_before_action :has_password, only: [:set_password, :create_password]

  ##
  # Form to create a new user from
  # GET /users/new
  def new
    @user = User.new
  end

  ##
  # Creates a new user
  # POST /users/new
  def create
    @user = User.new(create_params)
    if @user.valid_email?
      @user.save(validate: false)
      @user.send_activation_email
      flash[:success] = "Created user #{@user.email}"
      redirect_to(users_path)
    else
      render("new")
    end
  end

  ##
  # Page to display all users
  # GET /users
  def index
    @users = User.order(email: :asc)
  end

  ##
  # Page for users to manage the site
  # GET /manage
  def manage
  end

  ##
  # Deletes a user
  # DELETE /users/:id
  def destroy
    user = User.find(params[:id])
    user.delete
    flash[:success] = "Deleted user"
    redirect_to(users_path)
  end

  ##
  # Form for a new user to set their password
  # GET /profile/password/set
  def set_password
    @user = current_user
  end

  ##
  # Sets the user's password
  # PATCH /profile/password/set
  def create_password
    @user = current_user
    if @user.update_attributes(create_password_params)
      flash[:success] = "Successfully created your account"
      redirect_to(root_url)
    else
      render("set_password")
    end
  end

  ##
  # Form for user to change password
  # GET /profile/password/change
  def edit_password
    @user = current_user
  end

  ##
  # Changes the user's password
  # PATCH /profile/password/change
  def update_password
    @user  = current_user

    params = update_password_params
    if @user.authenticated?("password", params[:old_password])
      if params[:new_password].empty?
        @user.errors.add(:new_password, "cannot be empty.")
        render("edit_password")
      elsif @user.authenticated?("password", params[:new_password])
        @user.errors.add(:new_password, "cannot match old password.")
        render("edit_password")
      elsif @user.update_attributes({
          password: params[:new_password],
          password_confirmation: params[:password_confirmation]})
        flash[:success] = "Updated password"
        redirect_to(root_url)
      else
        render("edit_password")
      end
    else
      @user.errors.add(:old_password, "was incorrect.")
      render("edit_password")
    end
  end
  
  private

  ##
  # Parameters for creating a user
  def create_params
    params.require(:user).permit(:email)
  end

  ##
  # Parameters for creating a user's password
  def create_password_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  ##
  # Parameters for updating a password
  def update_password_params
    params.require(:user).permit(:old_password, :new_password,
                                 :password_confirmation)
  end

  ##
  # Redirect if given an invalid user
  def valid_user
    user = User.find_by(id: params[:id])
    if user.nil?
      flash[:danger] = "Could not find that user"
      redirect_to(users_path)
    end
  end

  ##
  # Redirect user if they have already set a password
  def no_password
    user = current_user
    if !user.password_digest.nil?
      redirect_to(profile_password_change_path)
    end
  end
end
