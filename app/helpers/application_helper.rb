module ApplicationHelper

  ##
  # Gets the title for the given page
  #
  # page:   optional title for the page
  #
  # Returns the page title and the base title formatted
  def page_title(page="")
    title = "UT ITF Taekwon-Do"
    if page != ""
      title = "#{page} | #{title}"
    end

    title
  end

  ##
  # Returns is-invalid if obj's errors include the given field
  def validation_class(obj, field)
    if obj.errors.include?(field)
      "is-invalid"
    else
      ""
    end
  end

  ##
  # Returns the error messages for the given field converted to a string
  def validation_message(obj, field)
    msg = obj.errors[field].join(", ")
    msg[0].capitalize + msg[1...msg.length]
  end

end
