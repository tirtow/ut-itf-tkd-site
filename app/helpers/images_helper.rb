module ImagesHelper

  def image_ok?(images, name)
    image = Image.get(name)
    !Rails.env.test? && images.include?(name) && image.photo.url
  end
end
