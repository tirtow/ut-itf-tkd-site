module SessionsHelper
 
  ##
  # Logs in a user
  def login(user)
    session[:user_id] = user.id
  end

  ##
  # Checks if there is a user currently logged in
  # Returns true if current_user is not nil, false otherwise
  def logged_in?
    !current_user.nil?
  end
 
  ##
  # Logs out the current user
  def logout
    session.delete(:user_id)
    @current_user = nil

    flash[:success] = "Successfully logged out"
  end
 
  ##
  # Gets the currently logged in user
  def current_user
    if session[:user_id]
      @current_user ||= User.find_by(id: session[:user_id])
    end
  end
end
