module TournamentsHelper

  ##
  # Formats the given datetime as mm/dd/yyyy
  def format_date(date)
    date.strftime("%m/%d/%Y")
  end

  ##
  # Gets the direction to sort in
  #
  # name:   the name to sort by
  # sort:   the current value being sorted by
  # dir:    the direction to sort if previously sorted by name
  #
  # Returns dir if name == sort, "asc" otherwise
  def sort_direction(name, sort, dir)
    if name == sort
      dir
    else
      "asc"
    end
  end

end
