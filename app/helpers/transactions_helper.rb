module TransactionsHelper

  ##
  # Calculates the number of revenue transactions
  #
  # transactions:   the transactions to count from
  #
  # Returns: the number of revenue transactions
  def revenue_count(transactions)
    transactions.where(spending: false).count
  end

  ##
  # Calculates the number of spending transactions
  #
  # transactions:   the transactions to count from
  #
  # Returns: the number of spending transactions
  def spending_count(transactions)
    transactions.where(spending: true).count
  end

  ##
  # Calculates the net amount
  #
  # transactions:   the transactions to calculate from
  #
  # Returns: the net amount
  def calc_net(transactions)
    calc_revenue(transactions) - calc_spending(transactions)
  end

  ##
  # Determines the net color
  #
  # transactions:   the transactions to determine from
  #
  # Returns: green if positive, red if negative, black if zero
  def net_color(transactions)
    net = calc_net(transactions)
    if net > 0 
      "green"
    elsif net < 0
      "red"
    else
      ""
    end
  end

  ##
  # Calculates the amount of revenue
  #
  # transactions:   the transactions to calculate from
  #
  # Returns: the amount of revenue
  def calc_revenue(transactions)
    sum = 0
    transactions.where(spending: false).each do |transaction|
      sum += transaction.amount.abs
    end

    sum
  end

  ##
  # Calculates the amount of spending
  #
  # transactions:   the transactions to calculate from
  #
  # Returns: the amount of spending
  def calc_spending(transactions)
    sum = 0
    transactions.where(spending: true).each do |transaction|
      sum += transaction.amount.abs
    end

    sum
  end
end
