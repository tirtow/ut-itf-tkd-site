module UsersHelper

  ##
  # Formats the given time to US Central Time
  #
  # time:   the DateTime to format
  #
  # Returns the formatted time string
  def format_time(time)
    time = time.in_time_zone("Central Time (US & Canada)")
    time.strftime("%b %d, %Y %H:%M %Z")
  end
end
