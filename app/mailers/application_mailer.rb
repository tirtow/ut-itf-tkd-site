class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@uttkd.org'
  layout 'mailer'
end
