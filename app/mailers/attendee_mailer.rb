class AttendeeMailer < ApplicationMailer
 
  ##
  # Mails registration confirmation
  def registration_confirmation(attendee)
    @attendee = attendee
    @tournament = attendee.tournament
    mail(to: attendee.email,
         subject: "#{@tournament.name} Registration Confirmation")
  end
end
