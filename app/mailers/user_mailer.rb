class UserMailer < ApplicationMailer

  ##
  # Mails account activation to user
  def account_activation(user)
    @user = user
    mail(to: user.email, subject: "UT TKD: Account Activation")
  end
end
