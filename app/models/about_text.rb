require 'redcarpet'

class AboutText < ApplicationRecord

  validates(:section, presence: true,
                      uniqueness: true)
  validates(:text,    presence: true)

  before_save :downcase_section

  def render_text
    AboutText.renderer.render(self.text)
  end

  ##
  # Get the AboutText with the given section name, case-insensitive
  #
  # section:  the name to lookup
  #
  # Returns the AboutText or nil if none found
  def AboutText.get(section)
    AboutText.find_by(section: section.downcase)
  end

  ##
  # Gets a hash mapping the section for an AboutText to the about text
  def AboutText.get_hash
    renderer = AboutText.renderer
    result = Hash.new
    AboutText.all.each do |about_text|
      result[about_text.section] = renderer.render(about_text.text)
    end

    result
  end

  private

  ##
  # Convert the section to lowercase
  def downcase_section
    self.section = section.downcase
  end

  ##
  # Gets the renderer to process markdown with
  def AboutText.renderer
    Redcarpet::Markdown.new(TargetBlankRenderer,
                            autolink: true,
                            tables: true,
                            filter_html: true,
                            no_images: true,
                            no_styles: true,
                            safe_links_only: true,
                            underline: true,
                            highlight: true,
                            strikethrough: true,
                            prettify: true)
  end

  ##
  # Inherits from renderer to make links open in new tab
  class TargetBlankRenderer < Redcarpet::Render::HTML
    def initialize(extensions = {})
      super extensions.merge(link_attributes: {target: "_blank"})
    end
  end
end
