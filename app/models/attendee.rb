class Attendee < ApplicationRecord

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  # Ranks an attendee can be
  RANKS = {
    0  => "White Belt",
    1  => "Yellow Stripe",
    2  => "Yellow Belt",
    3  => "Green Stripe",
    4  => "Green Belt",
    5  => "Blue Stripe",
    6  => "Blue Belt",
    7  => "Red Stripe",
    8  => "Red Belt",
    9  => "Black Stripe",
    10 => "1st Dan",
    11 => "2nd Dan",
    12 => "3rd Dan",
    13 => "4th Dan",
    14 => "5th Dan",
    15 => "6th Dan",
    16 => "7th Dan",
    17 => "8th Dan",
    18 => "9th Dan",
  }

  # Weight classes for black belts
  WEIGHT_CLASSES = {
    0 => "Micro Weight (-110, -99,-125.7, -110)",
    1 => "Light Weight (110-123.5, 99-110, 125.7-139, 110-123.5)",
    2 => "Middle Weight (123.5-136.7, 110-121, 139-154.3, 123.5-136.7)",
    3 => "Light Heavy Weight (136.7-150, 121-132.3, 154.3-172, 136.7-150)",
    4 => "Heavy Weight (150-165.4, 132.3-143.3, 172-187.4, 150-165.4)",
    5 => "Hyper Weight (+165.4, +143.3, +187.4, +165.4)"
  }

  # Names of each weight class
  WEIGHT_CLASS_NAMES = {
    0 => "Micro Weight",
    1 => "Light Weight",
    2 => "Middle Weight",
    3 => "Light Heavy Weight",
    4 => "Heavy Weight",
    5 => "Hyper Weight"
  }

  # Sex options
  SEXES = {
    0 => "Male",
    1 => "Female"
  }

  belongs_to :tournament

  validates(:first_name,      presence: true)
  validates(:last_name,       presence: true)
  validates(:rank,            presence: true)
  validates(:school,          presence: true)
  validates(:instructor,      presence: true)
  validates(:age,             presence: true,
                              numericality: {greater_than: 0})
  validates(:weight_class,    presence: true)
  validates(:sex,             presence: true)
  validates(:street,          presence: true)
  validates(:city,            presence: true)
  validates(:state,           presence: true)
  validates(:zip,             presence: true)
  validates(:phone,           presence: true)
  validates(:email,           presence: true,
                              length: {maximum: 255},
                              format: {with: VALID_EMAIL_REGEX})
  validates(:accepted_waiver, inclusion: {in: [true],
                                          message: "must be accepted"})

  ##
  # Gets the name of the rank of this attendee
  def rank_name
    RANKS[self.rank]
  end
 
  ##
  # Gets the name of the weight class of this attendee
  def weight_class_name
    WEIGHT_CLASS_NAMES[self.weight_class]
  end

  ##
  # Gets the name of the sex of this attendee
  def sex_name
    SEXES[self.sex]
  end

  ##
  # Gets the names of the events this attendee has signed up for
  def event_names
    events = []
    if self.patterns?
      events << "patterns"
    end

    if self.sparring?
      events << "sparring"
    end

    if self.breaking?
      events << "breaking"
    end

    if self.team_demo?
      events << "team demo"
    end

    events.join(", ")
  end

  ##
  # Send the confirmation email to this attendee
  def send_confirmation_email
    AttendeeMailer.registration_confirmation(self).deliver_now
  end

  ##
  # Adds this attendee to the given csv
  #
  # csv:  the csv to add to
  def to_csv(csv)
    result = []
    result << self.first_name
    result << self.last_name
    result << self.rank_name
    result << self.school
    result << self.instructor
    result << self.age
    result << self.weight_class_name
    result << self.sex_name
    result << self.patterns
    result << self.sparring
    result << self.breaking
    result << self.team_demo
    result << self.team_demo_name

    csv << result
  end

  def weak_match?(term)
    self.first_name.downcase.include?(term) ||
      self.last_name.downcase.include?(term)
  end

  def strong_match?(term)
    "#{self.first_name.downcase} #{self.last_name.downcase}".include?(term) ||
      "#{self.last_name.downcase} #{self.first_name.downcase}".include?(term)
  end

  ##
  # Gets the rank options for select
  #
  # include_none:   whether or not to include a "None" option. (default false)
  def Attendee.rank_select(include_none=false)
    Attendee.hash_to_select(RANKS, include_none)
  end

  ##
  # Gets the weight class options for select
  #
  # include_none:   whether or not to include a "None" option. (default false)
  def Attendee.weight_class_select(include_none=false)
    Attendee.hash_to_select(WEIGHT_CLASSES, include_none)
  end
 
  ##
  # Gets the weight class options for select
  #
  # include_none:   whether or not to include a "None" option. (default false)
  def Attendee.weight_class_name_select(include_none=false)
    Attendee.hash_to_select(WEIGHT_CLASS_NAMES, include_none)
  end

  ##
  # Gets the sex options for select
  #
  # include_none:   whether or not to include a "None" option. (default false)
  def Attendee.sex_select(include_none=false)
    Attendee.hash_to_select(SEXES, include_none)
  end

  ##
  # Gets the event options for select
  #
  # include_none:   whether or not to include a "None" option. (default false)
  def Attendee.events_select(include_none=false)
    result = [
      ["Patterns", "patterns"],
      ["Sparring", "sparring"],
      ["Board Breaking", "breaking"],
      ["Team Demo", "team_demo"]
    ]
    if include_none
      result = [["None", ""]] + result
    end

    result
  end

  private

  ##
  # Converts the given hash into an array for select where the value of the hash
  # is the value displayed in the select and the key of the hash is the actual
  # value of the select
  #
  # include_none:   whether or not to include a "None" option.
  def Attendee.hash_to_select(h, include_none)
    result = []
    if include_none
      result << ["None", ""]
    end

    h.each do |k, v|
      result << [v, k]
    end

    result
  end
end
