class ChartData

  ##
  # Gets the data for charts for yearly data
  #
  # Takes a block that takes in a transaction and returns true if the
  # transaction should be included and false otherwise
  #
  # spending:   whether or not the transactions should be spending transactions
  #
  # Returns: the data
  def ChartData.data_by_year(spending)
    data = {}
    Transaction.where(spending: spending).each do |transaction|
      if yield transaction
        if !data.include?(transaction.year)
          data[transaction.year] = 0
        end

        data[transaction.year] += transaction.amount.abs
      end
    end

    data
  end

  ##
  # Gets the data for charts for semesterly data
  #
  # Takes a block that takes in a transaction and returns true if the
  # transaction should be included and false otherwise
  #
  # spending:   whether or not the transactions should be spending transactions
  #
  # Returns: the data
  def ChartData.data_by_semester(spending)
    # Calculate due amounts for each year/semester
    result = {
      "Fall": {},
      "Spring": {},
      "Summer": {}
    }
    result.each do |semester, info|
      Transaction.where(spending: spending, semester: semester).each do |transaction|
        if yield transaction
          if !info.include?(transaction.year)
            info[transaction.year] = 0
          end

          info[transaction.year] += transaction.amount.abs
        end
      end
    end

    # Format for chartkick
    data = [
      {name: "Fall", data: []},
      {name: "Spring", data: []},
      {name: "Summer", data: []},
    ]

    data.each do |entry|
      result[entry[:name].to_sym].each do |key, value|
        entry[:data].push([key, value])
      end
    end
    
    data
  end

  ##
  # Gets the data for charts grouped by category split for separate graphs for
  # each year
  def ChartData.data_by_category_splits(by_category)
    data = {}

    by_category.each do |h|
      h[:data].each do |arr|
        if !data.include?(arr.first)
          data[arr.first] = {}
        end

        data[arr.first][h[:name]] = arr.last
      end
    end

    data
  end
end
