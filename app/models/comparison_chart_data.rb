class ComparisonChartData

  def ComparisonChartData.comparison_by_totals(negate=false)
    data = {
      "Revenue"  => 0,
      "Spending" => 0
    }

    Transaction.where(spending: false).each do |transaction|
      data["Revenue"] += transaction.amount
    end

    Transaction.where(spending: true).each do |transaction|
      if negate
        data["Spending"] += transaction.amount
      else
        data["Spending"] += transaction.amount.abs
      end
    end

    data
  end

  ##
  # Gets the comparison data for the year
  #
  # negate:   whehter or not to negate the spending values
  #
  # Returns: the data
  def ComparisonChartData.comparison_by_year(negate=false)
    data = [
      {name: "Revenue", data: []},
      {name: "Spending", data: []}
    ]

    RevenueChartData.revenue_by_year.each do |year, amount|
      data.first[:data].push([year, amount])
    end

    SpendingChartData.spending_by_year.each do |year, amount|
      if negate
        data.last[:data].push([year, amount * -1])
      else
        data.last[:data].push([year, amount])
      end
    end

    data
  end
end
