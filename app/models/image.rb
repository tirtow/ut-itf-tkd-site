class Image < ApplicationRecord

  mount_uploader :photo, ImageUploader

  before_save :downcase_name

  validates(:name,  presence: true, uniqueness: true)

  ##
  # Gets an Image given its name
  #
  # name:   the name to lookup
  #
  # Returns the Image or nil if not found
  def Image.get(name)
    Image.find_by(name: name.downcase)
  end

  ##
  # Gets a hash mapping the name of an image to the image
  def Image.get_hash
    result = Hash.new
    Image.all.each do |image|
      result[image.name] = image
    end

    result
  end

  private

  ##
  # Covert the name to lowercase
  def downcase_name
    self.name = name.downcase
  end
end
