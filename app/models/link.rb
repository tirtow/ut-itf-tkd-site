class Link < ApplicationRecord
  VALID_URL_REGEX  = /\A(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\z/ix

  before_save :downcase_name

  validates(:name, presence: true,
                   uniqueness: {case_sensitive: false})
  validates(:url,  presence: true,
                   format: {with: VALID_URL_REGEX,
                            message: "must be a valid URL"})

  ##
  # Get a link given its name
  # name:   the name of the link
  def Link.get(name)
    Link.find_by(name: name.downcase)
  end

  ##
  # Gets a hash mapping the url for a link to its name
  def Link.get_hash
    result = Hash.new
    Link.all.each do |link|
      result[link.name] = link.url
    end

    result
  end

  private

  ##
  # Converts the link's name to all lowercase
  def downcase_name
    self.name = name.downcase
  end
end
