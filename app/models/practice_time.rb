class PracticeTime < ApplicationRecord

  DAYS_OF_WEEK = {
    0 => "Monday",
    1 => "Tuesday",
    2 => "Wednesday",
    3 => "Thursday",
    4 => "Friday",
    5 => "Saturday",
    6 => "Sunday"
  }

  validates(:day,      presence: true)
  validates(:time,     presence: true)
  validates(:location, presence: true)
  
  ##
  # Gets the name of the day of the week
  def day_name
    DAYS_OF_WEEK[self.day]
  end

  ##
  # Gets the select options for the days of the week
  def PracticeTime.day_select
    result = Array.new
    DAYS_OF_WEEK.each {|num, name| result << [name, num]}
    result
  end

end
