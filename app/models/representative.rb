require 'carrierwave/orm/activerecord'

class Representative < ApplicationRecord
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  PRESIDENT = 0
  VP = 1
  REP = 2

  POSITIONS = {
    PRESIDENT => "President",
    VP => "Vice President",
    REP => "Representative"
  }

  # Causing tests to fail
  if !Rails.env.test?
    mount_uploader :photo, RepresentativePhotoUploader
    validate(:check_dimensions, on: :create)
  end

  validates(:name,     presence: true)
  validates(:email,    presence: true,
                       length: {maximum: 255},
                       format: {with: VALID_EMAIL_REGEX},
                       uniqueness: {case_sensitive: false})
  validates(:position, presence: true)
  validates(:photo,    presence: true)
  validate(:position_available)

  ##
  # Gets the name of the position of this representative
  def position_name
    POSITIONS[self.position]
  end

  ##
  # Gets the select options for representative positions
  def Representative.position_select
    result = []
    POSITIONS.each do |id, position|
      result << [position, id]
    end

    result
  end

  private

  ##
  # Validates the dimensions of the uploaded photo
  def check_dimensions
    if !photo_cache.nil? && (photo.width != 640 || photo.height != 480)
      errors.add(:photo, "must be 640x480 pixels")
    end
  end

  ##
  # Validates that a representative can be added to this position
  #
  # Fails if the position is for the President or Vice-President and a
  # Representative for that position already exists and is not this
  # Representative
  def position_available
    if position != REP
      rep = Representative.find_by(position: position)
      if !rep.nil? && rep != self
        errors.add(:position, "has already been filled")
      end
    end
  end
end
