class RevenueChartData

  ##
  # Gets the data for revenue charts for all revenue by year
  def RevenueChartData.revenue_by_year
    RevenueChartData.data_by_year {|t| true}
  end
 
  ##
  # Gets the data for revenue charts for all revenue by semester
  def RevenueChartData.revenue_by_semester
    RevenueChartData.data_by_semester {|t| true}
  end

  ##
  # Gets the data for revenue charts grouped by category
  def RevenueChartData.revenue_by_category
    data = {
      "Allocation" => 0,
      "Donations" => 0,
      "Dues" => 0,
      "Tournament" => 0,
      "Misc" => 0
    }

    Transaction.where(spending: false).each do |transaction|
      if transaction.allocation?
        data["Allocation"] += transaction.amount.abs
      elsif transaction.donation?
        data["Donations"] += transaction.amount.abs
      elsif transaction.dues?
        data["Dues"] += transaction.amount.abs
      elsif transaction.tournament?
        data["Tournament"] += transaction.amount.abs
      elsif transaction.misc_revenue?
        data["Misc"] += transaction.amount.abs
      end
    end

    data
  end

  ##
  # Gets the data for revenue charts grouped by category and year
  def RevenueChartData.revenue_by_category_and_year
    # Calculate the amounts for each category for each year
    data = {
      "Allocation" => {},
      "Donations" => {},
      "Dues" => {},
      "Tournament" => {},
      "Misc" => {}
    }
    Transaction.where(spending: false).each do |transaction|
      key = nil
      if transaction.allocation?
        key = "Allocation"
      elsif transaction.donation?
        key = "Donations"
      elsif transaction.dues?
        key = "Dues"
      elsif transaction.tournament?
        key = "Tournament"
      elsif transaction.misc_revenue?
        key = "Misc"
      end

      if !key.nil?
        if !data[key].include?(transaction.year)
          data[key][transaction.year] = 0
        end

        data[key][transaction.year] += transaction.amount.abs
      end
    end

    # Convert to format for chartkick
    result = [
      {name: "Allocation", data: []},
      {name: "Donations", data: []},
      {name: "Dues", data: []},
      {name: "Tournament", data: []},
      {name: "Misc", data: []}
    ]

    result.each do |entry|
      data[entry[:name]].each do |key, value|
        entry[:data].push([key, value])
      end
    end

    result
  end

  ##
  # Gets the revenue data for charts grouped by category split for separate
  # graphs for each year
  def RevenueChartData.revenue_by_category_splits
    ChartData.data_by_category_splits(
      RevenueChartData.revenue_by_category_and_year)
  end

  ##
  # Gets the data for revenue charts for dues by year
  def RevenueChartData.dues_by_year
    RevenueChartData.data_by_year {|t| t.dues?}
  end

  ##
  # Gets the data for revenue charts for dues by semester
  def RevenueChartData.dues_by_semester
    RevenueChartData.data_by_semester {|t| t.dues?}
  end

  ##
  # Gets the data for revenue charts for tournament entry fees by year
  def RevenueChartData.tournament_by_year
    RevenueChartData.data_by_year {|t| t.tournament?}
  end

  ##
  # Gets the data for revenue charts for donations by year
  def RevenueChartData.donations_by_year
    RevenueChartData.data_by_year {|t| t.donation?}
  end

  ##
  # Gets the data for revenue charts for donations by semester
  def RevenueChartData.donations_by_semester
    RevenueChartData.data_by_semester {|t| t.donation?}
  end

  ##
  # Gets the data for revenue charts for allocations by year
  def RevenueChartData.allocations_by_year
    RevenueChartData.data_by_year {|t| t.allocation?}
  end

  ##
  # Gets the data for revenue charts for miscellaneous revenue by year
  def RevenueChartData.misc_by_year
    RevenueChartData.data_by_year {|t| t.misc_revenue?}
  end

  private

  ##
  # Gets the data for revenue charts for yearly data
  #
  # Takes a block that takes in a transaction and returns true if the
  # transaction should be included and false otherwise
  #
  # Returns: the data
  def RevenueChartData.data_by_year
    ChartData.data_by_year(false) {|t| yield t}
  end

  ##
  # Gets the data for revenue charts for semesterly data
  #
  # Takes a block that takes in a transaction and returns true if the
  # transaction should be included and false otherwise
  #
  # Returns: the data
  def RevenueChartData.data_by_semester
    ChartData.data_by_semester(false) {|t| yield t}
  end
end
