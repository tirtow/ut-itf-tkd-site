class SpendingChartData

  ##
  # Gets the data for spending charts for all spending by year
  def SpendingChartData.spending_by_year
    SpendingChartData.data_by_year {|t| true}
  end

  ##
  # Gets the data for spending charts for all spending by semester
  def SpendingChartData.spending_by_semester
    SpendingChartData.data_by_semester {|t| true}
  end

  ##
  # Gets the data for spending charts by category
  def SpendingChartData.spending_by_category
    SpendingChartData.data_by_category {|t| true}
  end

  ##
  # Gets the spending data for charts by category and year
  def SpendingChartData.spending_by_category_and_year
    SpendingChartData.data_by_category_and_year {|t| true}
  end

  ##
  # Gets the spending data for charts grouped by category split for separate
  # graphs for each year
  def SpendingChartData.spending_by_category_splits
    ChartData.data_by_category_splits(
      SpendingChartData.spending_by_category_and_year)
  end

  private

  ##
  # Gets the data for spending charts for yearly data
  #
  # Takes a block that takes in a transaction and returns true if the
  # transaction should be included and false otherwise
  #
  # Returns: the data
  def SpendingChartData.data_by_year
    ChartData.data_by_year(true) {|t| yield t}
  end

  ##
  # Gets the data for spending charts for semesterly data
  #
  # Takes a block that takes in a transaction and returns true if the
  # transaction should be included and false otherwise
  #
  # Returns: the data
  def SpendingChartData.data_by_semester
    ChartData.data_by_semester(true) {|t| yield t}
  end

  ##
  # Gets the data for spending charts for data by category
  #
  # Takes a block that takes in a transaction and returns true if the
  # transaction should be included and false otherwise
  #
  # Returns: the data
  def SpendingChartData.data_by_category
    data = {}
    Transaction.where(spending: true).each do |transaction|
      if yield transaction
        if !data.include?(transaction.category)
          data[transaction.category] = 0
        end

        data[transaction.category] += transaction.amount.abs
      end
    end

    data
  end

  ##
  # Gets the data for spending charts for data by year and category
  #
  # Takes a block that takes in a transaction and returns true if the
  # transaction should be included and false otherwise
  #
  # Returns: the data
  def SpendingChartData.data_by_category_and_year
    # Calculate due amounts for each year/semester
    result = {
      "Apparel": {},
      "Atheltic": {},
      "Training": {},
      "Conference/League Dues": {},
      "Equipment": {},
      "Facility Rental": {},
      "Insurance": {},
      "Meals": {},
      "Official/Judges Fees": {},
      "Payroll": {},
      "Printing/Duplication": {},
      "Retail Sales Tax": {},
      "Travel": {},
      "Fringe Benefits": {}
    }

    result.each do |category, info|
      Transaction.where(spending: true, category: category).each do |transaction|
        if yield transaction
          if !info.include?(transaction.year)
            info[transaction.year] = 0
          end

          info[transaction.year] += transaction.amount.abs
        end
      end
    end

    # Format for chartkick
    data = [
      {name: "Apparel",                data: []},
      {name: "Atheltic",               data: []},
      {name: "Training",               data: []},
      {name: "Conference/League Dues", data: []},
      {name: "Equipment",              data: []},
      {name: "Facility Rental",        data: []},
      {name: "Insurance",              data: []},
      {name: "Meals",                  data: []},
      {name: "Official/Judges Fees",   data: []},
      {name: "Payroll",                data: []},
      {name: "Printing/Duplication",   data: []},
      {name: "Retail Sales Tax",       data: []},
      {name: "Travel",                 data: []},
      {name: "Fringe Benefits",        data: []}
    ]

    data.each do |entry|
      result[entry[:name].to_sym].each do |key, value|
        entry[:data].push([key, value])
      end
    end

    # Delete entries with no amount
    data.delete_if do |entry|
      amount = 0
      entry[:data].each do |info|
        amount += info.last
      end

      amount == 0
    end
    
    data
  end
end
