class TestingSheet < ApplicationRecord

  mount_uploader :file, TestingSheetUploader

  before_save :downcase_name

  validates(:name, presence: true, uniqueness: true)

  ##
  # Gets a TestingSheet given its name
  #
  # name:   the name to lookup
  #
  # Returns the TestingSheet or nil if not found
  def TestingSheet.get(name)
    TestingSheet.find_by(name: name.downcase)
  end

  private

  ##
  # Covert the name to lowercase
  def downcase_name
    self.name = name.downcase
  end
end
