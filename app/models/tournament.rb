require 'csv'

class Tournament < ApplicationRecord

  VALID_PATH_REGEX = /\A[a-zA-Z0-9\-]+\z/
  VALID_URL_REGEX  = /\A(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?\z/ix

  DEFAULT_PAYMENT_LINK = "https://paypal.me/uttkd"
  DEFAULT_CHECKIN_TIME = "8:00 am"
  DEFAULT_START_TIME   = "9:30 am"

  has_many :attendees

  before_save :downcase_path

  validates(:name,         presence: true)
  validates(:event_date,   presence: true)
  validates(:path,         presence: true,
                           uniqueness: {case_sensitive: false})
  validates(:path,         format: {with: VALID_PATH_REGEX,
                                    message: "may only contain letters, " +
                                             "numbers, and \"-\""})
  validates(:payment_link, presence: true,
                           format: {with: VALID_URL_REGEX,
                                    message: "must be a valid URL"})
  validates(:checkin_time, presence: true)
  validates(:start_time,   presence: true)

  ##
  # Gets the attendees who's first or last name match one of the terms entered
  # to search by.
  #
  # terms:          the term to filter by. (default "")
  # rank:           the rank to filter by. (default "")
  # weight_class:   the rank to filter by. (default "")
  # event:          the rank to filter by. (default "")
  def filter_attendees(terms="", rank="", weight_class="", event="")
    result = self.attendees
    if !rank.blank?
      result = result.where(rank: rank)
    end

    if !weight_class.blank?
      result = result.where(weight_class: weight_class)
    end

    if !event.blank?
      result = result.where(event => true)
    end

    terms = terms.downcase.split(/\s+/)
    if terms.empty?
      result.order(last_name: :asc, first_name: :asc)
    else
      new_result = strong_matches(result, terms)
      if new_result.empty?
        result = weak_matches(result, terms)
      else
        result = new_result
      end

      result.sort_by! do |arr|
        [-arr[1], arr[0].last_name, arr[0].last_name]
      end

      result.map {|arr| arr.first}
    end
  end

  ##
  # Generates a CSV of the attendees for this tournament
  def to_csv
    CSV.generate(headers: true) do |csv|
      csv << ["first_name", "last_name", "rank_name", "school", "instructor",
              "age", "weight_class", "sex", "patterns", "sparring", "breaking",
              "team_demo", "team_demo_name"]
      self.attendees.each {|attendee| attendee.to_csv(csv)}
    end
  end

  ##
  # Gets the name to use for this tournament's CSV
  def csv_name
    "#{DateTime.now.strftime("%Y%m%d%H%M")}_#{self.name.gsub(/\s/, "_")}.csv"
  end

  ##
  # Gets a tournament by its path, case-insensitive
  # 
  # path:   the path to lookup
  #
  # Returns the Tournament or nil if no tournament found
  def Tournament.get(path)
    Tournament.find_by(path: path.downcase)
  end

  private

  ##
  # Converts the path to lowercase
  def downcase_path
    self.path = path.downcase
  end

  def strong_matches(result, terms)
    result = result.map {|attendee| [attendee, 0]}
    full_term = terms.join(" ")
    result.each do |arr|
      if arr[0].strong_match?(full_term)
        arr[1] += 1
      end
    end

    result.keep_if {|arr| arr[1] != 0}
    result
  end

  def weak_matches(result, terms)
    result = result.map {|attendee| [attendee, 0]}
    terms.each do |term|
      result.each do |arr|
        if arr[0].weak_match?(term)
          arr[1] += 1
        end
      end
    end

    result.keep_if {|arr| arr[1] != 0}
    result
  end
end
