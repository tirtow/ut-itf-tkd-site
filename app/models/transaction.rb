class Transaction < ApplicationRecord

  validates(:date,           presence: true)
  validates(:transaction_id, presence: true, uniqueness: true)
  validates(:amount,         presence: true)
  validates(:spending,       inclusion: {in: [true, false]})
  validates(:category,       presence: true)
  validates(:description,    exclusion: {in: [nil]})
  validates(:account,        presence: true,
                             inclusion: {in: ["studentorg",
                                              "allocation",
                                              "donation"]})
  validates(:semester,       presence: true)
  validates(:year,           presence: true)

  KEYS = [:date, :transaction_id, :amount, :type, :category, :description]

  ##
  # Gets whether or not this transaction is a dues transaction
  #
  # Returns: true if dues, false otherwise
  def dues?
    !self.spending && self.description.downcase.include?("dues")
  end

  ##
  # Gets whether or not this transaction is a tournament entry fee deposit
  #
  # Returns: true if tournament entry fee, false otherwise
  def tournament?
    !self.spending && !self.dues? &&
     (self.description.downcase.include?("entry fee") ||
      (self.date.month == 2 || self.date.month == 3) &&
       self.description.downcase.include?("deposit"))
  end

  ##
  # Gets whether or not this transaction is the yearly allocation
  #
  # Returns: true if yearly allocation, false otherwise
  def allocation?
    !self.spending && self.account == "allocation" &&
     !self.description.downcase.include?("adjust")
  end

  ##
  # Gets whether or not this transaction is a donation
  #
  # Returns: true if donation, false otherwise
  def donation?
    !self.spending && self.account == "donation"
  end

  ##
  # Gets whether or not this transaction is not any other kind of revenue
  # transaction
  #
  # Returns: true if miscellaneous, false otherwise
  def misc_revenue?
    !(self.dues? || self.tournament? || self.allocation? || self.donation?)
  end

  ##
  # Sets the semester for this transaction
  #
  # Returns: the semester
  def determine_semester
    month = self.date.month
    if 1 <= month && month <= 5
      "Spring"
    elsif 6 <= month && month <= 8
      "Summer"
    else
      "Fall"
    end
  end

  ##
  # Sets the academic year for this transaction
  #
  # If the month of the date is January - August the school year is set to the
  # previous year.
  #
  # Returns: the academic year
  def determine_academic_year
    year = self.date.year
    if self.date.month < 9
      year -= 1
    end

    if self.allocation?
      year += 1
    end

    year
  end

  ##
  # Builds all transactions from the give page
  #
  # page:   the page to build from
  def Transaction.build_all(page)
    # Get the account
    account = Transaction.account(page)

    # Iterate through each row
    cancelled = []
    page.css("tbody tr").each do |row|
      # Build the info hash for the row
      info = {}
      row.css("td").each_with_index do |td, index|
        info[KEYS[index]] = td.text
      end

      # Ignore rows that do not supply the full info
      if info.keys.sort == KEYS.sort
        Transaction.build(info, account, cancelled)
      end
    end
  end

  ##
  # Builds and saves the transaction given the info for the tranaction
  #
  # Transactions show up twice if they are cancelled. So need to keep track of
  # which transactions have been cancelled and check that before saving a
  # transaction. Whenever come across a cancelled transaction, check the db for
  # any saved transactions with the same transaction ID and delete those.
  #
  # info:       the hash to build with
  # account:    the account the transaction is for
  # cancelled:  the Array of cancelled transactions
  #
  # Returns: the transaction or nil if failed to save
  def Transaction.build(info, account, cancelled)
    transaction = Transaction.new
    
    # Copy attributes
    transaction.transaction_id = info[:transaction_id]
    transaction.account = account
    transaction.category = info[:category].sub("Expenses - ", "")
    transaction.description = info[:description]
    transaction.spending = info[:type] == "Payment"

    # Parse the amount
    transaction.amount = Transaction.parse_money(info[:amount])

    # Set the date
    transaction.date = DateTime.strptime(info[:date], "%m/%d/%y")
    transaction.semester = transaction.determine_semester
    transaction.year = transaction.determine_academic_year

    if transaction.category == "Cancelled"
      # The transaction was cancelled, delete any transaction with the same
      # transaction ID and stop
      cancelled.push(transaction.transaction_id)
      record = Transaction.find_by(transaction_id: transaction.transaction_id)
      if !record.nil?
        record.delete
      end

      nil
    elsif !cancelled.include?(transaction.transaction_id) && transaction.save
      transaction
    else
      nil
    end
  end

  ##
  # Gets the account name from the page
  #
  # page:   the html page to parse
  #
  # Returns: the account name or nil if could not find
  def Transaction.account(page)
    rows = page.css("strong")
    rows.each do |row|
      if row.text.include?("Taekwon-Do ITF")
        # The name is formatted like "Taekwon-Do ITF - Some Name Account"
        # Parse it to be "somename"
        account = row.text.sub!("Taekwon-Do ITF - ", "")
        account.sub!("Account", "")
        account.sub!(/\s/, "")
        account.strip!
        return account.downcase
      end
    end

    nil
  end

  ##
  # Parses the amount into a float
  #
  # amount:   the amount to parse
  #
  # Returns: the float amount
  def Transaction.parse_money(amount)
    amount.sub!("$", "")
    amount.sub!(",", "")
    amount.to_f
  end
end
