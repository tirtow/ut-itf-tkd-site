class User < ApplicationRecord

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i

  attr_accessor :activation_token

  has_secure_password

  before_save :downcase_email

  validates(:email,    presence: true,
                       length: {maximum: 255},
                       format: {with: VALID_EMAIL_REGEX},
                       uniqueness: {case_sensitive: false})
  validates(:password, presence: true,
                       length: {minimum: 8},
                       allow_nil: true)

  ##
  # Authenticates the given token
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    if digest.nil?
      return false
    else
      BCrypt::Password.new(digest).is_password?(token)
    end
  end

  ##
  # Returns true if the email is valid, false otherwise
  def valid_email?
    self.valid?
    self.errors.delete(:password)
    self.errors[:email].blank?
  end

  ##
  # Sends the activation email for this user
  def send_activation_email
    if !self.activated?
      self.activation_token = User.new_token
      self.update_attribute(:activation_digest,
                            User.digest(self.activation_token))
      UserMailer.account_activation(self).deliver_now
    end
  end

  ##
  # Activates this user's account
  def activate
    if !self.activated?
      self.update_attribute(:activated, true)
      self.update_attribute(:activated_at, DateTime.now)
    end
  end

  ##
  # Creates a digest from the given string using BCrypt
  # string:   the string to digest
  # Returns the digest
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  ##
  # Generates a new url safe token
  def User.new_token
    SecureRandom.urlsafe_base64
  end
  
  private

  ##
  # Converts the user's email to all lowercase
  def downcase_email
    self.email = email.downcase
  end

end
