class ImageUploader < CarrierWave::Uploader::Base
  storage :file

  ##
  # Set filename with secure_token
  def filename
    "#{model.name}.#{file.extension}" if original_filename.present?
  end

  ##
  # Set directory to store in
  def store_dir
    "uploads/#{model.class.to_s.underscore}"
  end
 
  ##
  # Set allowed extensions
  def extension_whitelist
    %w(jpg jpeg gif png)
  end
end
