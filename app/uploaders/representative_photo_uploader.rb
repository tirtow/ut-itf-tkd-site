class RepresentativePhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  attr_reader :width, :height
  before :cache, :capture_size

  storage :file

  ##
  # Set filename with secure_token
  def filename
    "#{secure_token}.#{file.extension}" if original_filename.present?
  end

  ##
  # Set directory to store in
  def store_dir
    "uploads/#{model.class.to_s.underscore}"
  end
 
  ##
  # Set allowed extensions
  def extension_whitelist
    %w(jpg jpeg gif png)
  end

  private

  ##
  # Set the dimensions of the image
  def capture_size(file)
    if file.path.nil?
      img = ::MiniMagick::Image::read(file.file)
      @width = img[:width]
      @height = img[:height]
    else
      @width, @height = `identify -format "%wx %h" #{file.path}`.split(/x/).map{|dim| dim.to_i }
    end
  end

  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end
end
