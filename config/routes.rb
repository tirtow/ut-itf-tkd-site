Rails.application.routes.draw do
  root "static_pages#home"

  # Tournaments routes
  get   "/tournaments/new",        to: "tournaments#new"
  post  "/tournaments/new",        to: "tournaments#create"
  get   "/tournaments",            to: "tournaments#index"
  get   "/tournaments/:id"         =>  "tournaments#show", as: "tournament"
  patch "/tournaments/:id/open"    =>  "tournaments#open_registration",
         as: "tournaments_open"
  patch "/tournaments/:id/close"   =>  "tournaments#close_registration",
         as: "tournaments_close"
  get   "/tournaments/:id/checkin" =>  "tournaments#checkin",
         as: "tournaments_checkin"
  get   "/tournaments/:id/manage"  =>  "tournaments#manage",
         as: "tournaments_manage"
  get   "/tournaments/:id/edit"    =>  "tournaments#edit",
         as: "tournaments_edit"
  patch "/tournaments/:id/edit"    =>  "tournaments#update",
         as: "tournaments_update"
  get   "/tournaments/:id/csv"     =>  "tournaments#generate_csv",
         as: "tournaments_csv"
  get   "/tournaments/:id/checkin/filter" => "tournaments#filter_checkin",
         as: "tournaments_checkin_filter"
  get   "/tournaments/:id/attendees"      => "tournaments#show_attendees",
         as: "tournaments_attendees_show"
  get   "/tournaments/:id/attendees/filter" => "tournaments#filter_attendees",
         as: "tournaments_attendees_filter"

  # AboutText routes
  get   "/texts",              to: "about_texts#index"
  get   "/texts/:section/edit" =>  "about_texts#edit",   as: "text_edit"
  patch "/texts/:section/edit" =>  "about_texts#update", as: "text_update"

  # Attendees routes
  get  "/tournaments/:id/register"                          => "attendees#new",
        as: "attendees_new"
  post "/tournaments/:id/register"                          => "attendees#create",
        as: "attendees_create"
  get   "/tournaments/:id/register/confirmation"            => "attendees#confirm",
         as: "attendees_confirmation"
  get  "/tournaments/:id/attendees/:attendee_id"            => "attendees#show",
        as: "attendee"
  patch "/tournaments/:id/attendees/:attendee_id/checkin"   => "attendees#checkin",
         as: "attendees_checkin"
  patch "/tournaments/:id/attendees/:attendee_id/uncheckin" => "attendees#uncheckin",
         as: "attendees_uncheckin"
  patch "/tournaments/:id/attendees/:attendee_id/paid"      => "attendees#paid",
         as: "attendees_paid"
  patch "/tournaments/:id/attendees/:attendee_id/unpaid"    => "attendees#unpaid",
         as: "attendees_unpaid"

  # Images routes
  get   "/images",         to: "images#index"
  get   "/images/:id/edit" =>  "images#edit",   as: "images_edit"
  patch "/images/:id/edit" =>  "images#update", as: "images_update"

  # Links routes
  get   "/donate",             to: "links#donate"
  get   "/40for40",            to: "links#donate"
  get   "/dues/non-traveling", to: "links#dues_regular"
  get   "/dues/traveling",     to: "links#dues_traveling"
  get   "/volunteer",          to: "links#volunteer"
  get   "/join",               to: "links#join"
  get   "/links",              to: "links#index"
  get   "/links/:name/edit"    =>  "links#edit", as: "link_edit"
  patch "/links/:name/edit"    =>  "links#update", as: "link_update"

  # PracticeTimes routes
  get    "/practices",     to: "practice_times#index"
  get    "/practices/new", to: "practice_times#new"
  post   "/practices/new", to: "practice_times#create"
  delete "/practices/:id"  =>  "practice_times#destroy", as: "practices_delete"

  # Representatives routes
  get    "/representatives/new",     to: "representatives#new"
  post   "/representatives/new",     to: "representatives#create"
  get    "/representatives",         to: "representatives#index"
  delete "/representatives/:id"      =>  "representatives#destroy",
          as: "representatives_delete"
  get    "/representatives/:id/edit" =>  "representatives#edit",
          as: "representatives_edit"
  patch  "/representatives/:id/edit" =>  "representatives#update",
          as: "representatives_update"

  # Sessions routes
  get    "/login",  to: "sessions#new"
  post   "/login",  to: "sessions#create"
  delete "/logout", to: "sessions#destroy"

  # StaticPages routes
  get "/dues", to: "static_pages#dues"

  # Testing routes
  get   "/testing",                to: "testings#testing"
  get   "/testing/sheets/edit",    to: "testings#index"
  get   "/testing/sheets/:id/edit" =>  "testings#edit", as: "testing_edit"
  patch "/testing/sheets/:id/edit" =>  "testings#update", as: "testing_update"

  # Transaction routes
  get  "/transactions",        to: "transactions#home"
  get  "/transactions/update", to: "transactions#update"
  post "/transactions/scrape", to: "transactions#scrape"
  get  "/charts/revenue",      to: "transactions#revenue_charts"
  get  "/charts/spending",     to: "transactions#spending_charts"
  get  "/charts/comparison",   to: "transactions#comparison_charts"

  # Users routes
  get    "/manage",                  to: "users#manage"
  get    "/profile/password/change", to: "users#edit_password"
  patch  "/profile/password/change", to: "users#update_password"
  get    "/profile/password/set",    to: "users#set_password"
  patch  "/profile/password/set",    to: "users#create_password"
  get    "/users/new",               to: "users#new"
  post   "/users/new",               to: "users#create"
  get    "/users",                   to: "users#index"
  delete "/users/:id"                =>  "users#destroy", as: "users_delete"

  # Resources
  resources :account_activations, only: [:edit]
end
