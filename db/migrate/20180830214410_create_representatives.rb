class CreateRepresentatives < ActiveRecord::Migration[5.2]
  def change
    create_table :representatives do |t|
      t.string  :name
      t.string  :email
      t.string  :photo
      t.integer :position

      t.timestamps
    end
  end
end
