class CreateAttendees < ActiveRecord::Migration[5.2]
  def change
    create_table :attendees do |t|
      t.string :first_name
      t.string :last_name
      t.integer :rank
      t.string :school
      t.string :instructor
      t.boolean :patterns
      t.boolean :sparring
      t.boolean :breaking
      t.integer :age
      t.integer :weight_class
      t.boolean :paid, default: false
      t.boolean :checked_in, default: false
      t.integer :sex
      t.string :street
      t.string :city
      t.integer :state
      t.string :zip
      t.string :phone
      t.string :email
      t.boolean :team_demo
      t.string :team_demo_name
      t.boolean :accepted_waiver, default: false

      t.timestamps
    end
  end
end
