class ChangeStateToStringInAttendees < ActiveRecord::Migration[5.2]
  def change
    remove_column :attendees, :state
    add_column    :attendees, :state, :string
  end
end
