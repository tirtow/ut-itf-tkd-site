class AddTournamentIdToAttendees < ActiveRecord::Migration[5.2]
  def change
    add_column :attendees, :tournament_id, :integer
  end
end
