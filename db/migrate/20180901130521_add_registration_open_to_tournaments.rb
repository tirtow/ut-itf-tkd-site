class AddRegistrationOpenToTournaments < ActiveRecord::Migration[5.2]
  def change
    add_column :tournaments, :registration_open, :boolean, default: false
  end
end
