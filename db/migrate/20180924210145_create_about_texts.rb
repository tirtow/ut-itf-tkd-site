class CreateAboutTexts < ActiveRecord::Migration[5.2]
  def change
    create_table :about_texts do |t|
      t.string :text
      t.string :section

      t.timestamps
    end
  end
end
