class CreatePracticeTimes < ActiveRecord::Migration[5.2]
  def change
    create_table :practice_times do |t|
      t.string :day
      t.string :time
      t.string :location

      t.timestamps
    end
  end
end
