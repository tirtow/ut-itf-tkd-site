class ChangeTypeOfDayForPracticeTimes < ActiveRecord::Migration[5.2]
  def change
    remove_column :practice_times, :day
    add_column    :practice_times, :day, :integer
  end
end
