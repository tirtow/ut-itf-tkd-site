class AddPathToTournaments < ActiveRecord::Migration[5.2]
  def change
    add_column :tournaments, :path, :string
  end
end
