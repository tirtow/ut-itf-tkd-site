class AddOptionsToTournaments < ActiveRecord::Migration[5.2]
  def change
    add_column :tournaments, :payment_link, :string
    add_column :tournaments, :registration_open_time, :string
    add_column :tournaments, :tournament_start_time, :string
  end
end
