class ChangeRegistrationOpenTimeToCheckinTimeForTournaments < ActiveRecord::Migration[5.2]
  def change
    rename_column :tournaments, :registration_open_time, :checkin_time
    rename_column :tournaments, :tournament_start_time, :start_time
  end
end
