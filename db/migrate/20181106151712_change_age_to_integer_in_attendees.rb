class ChangeAgeToIntegerInAttendees < ActiveRecord::Migration[5.2]
  def change
    change_column :attendees, :age, :integer
  end
end
