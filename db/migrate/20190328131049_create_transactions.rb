class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.datetime :date
      t.integer :transaction_id
      t.decimal :Amount
      t.boolean :spending
      t.string :category
      t.string :description

      t.timestamps
    end
  end
end
