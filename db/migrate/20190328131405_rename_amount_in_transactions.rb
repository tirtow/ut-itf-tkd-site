class RenameAmountInTransactions < ActiveRecord::Migration[5.2]
  def change
    rename_column :transactions, :Amount, :amount
  end
end
