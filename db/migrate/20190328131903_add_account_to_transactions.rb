class AddAccountToTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :account, :string
  end
end
