class AddYearSemesterToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :year, :integer
    add_column :transactions, :semester, :string
  end
end
