class CreateTestingSheets < ActiveRecord::Migration[5.2]
  def change
    create_table :testing_sheets do |t|
      t.string :name
      t.string :file

      t.timestamps
    end
  end
end
