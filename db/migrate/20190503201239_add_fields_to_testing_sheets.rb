class AddFieldsToTestingSheets < ActiveRecord::Migration[5.2]
  def change
    add_column :testing_sheets, :title, :string
    add_column :testing_sheets, :order, :integer
  end
end
