class AddPathToLinks < ActiveRecord::Migration[5.2]
  def change
    add_column :links, :path, :string
  end
end
