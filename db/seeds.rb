if AboutText.find_by(section: "about_1").nil?
  AboutText.create(section: "about_1",
                   text: "We are the ITF Taekwon-Do club at the **University of Texas at Austin**. Our club is a part of the [American Taekwon-Do Federation International (ATFI)] (http://atfi-itf.com/) and the [International Taekwon-Do Federation (ITF)](https://www.taekwondoitf.org/). We cover the entirety of ITF Taekwon-Do in our practices including fundamental exercises, patterns, body conditioning, self-defense, sparring, and board breaking.")
end

if AboutText.find_by(section: "about_2").nil?
  AboutText.create(section: "about_2",
                   text: "The club participates in competitions throughout the United States and has sent members to Europe, South America, Canada, and Korea to represent the USA and the University of Texas.")
end

if AboutText.find_by(section: "about_3").nil?
  AboutText.create(section: "about_3",
                   text: "The club was founded in 1991 by Master Mike Stinson and is currently coached by Master Travis G. Young, a 7th Dan Black Belt and internationally certified instructor of Taekwon-Do ITF with over 35 years of experience.")
end

if AboutText.find_by(section: "about_4").nil?
  AboutText.create(section: "about_4",
                   text: "Beginners are always welcome. Most of our members start off with little or no experience and several have gone on to receive their black belts before graduating. The first four practices are free after that dues are only $40 for the semester. Come by and try it out!")
end

if AboutText.find_by(section: "schedule").nil?
  AboutText.create(section: "schedule",
                   text: "Our Monday and Wednesday practices focus primarily on technique where we work on patterns, step sparring, and self-defense.  Saturdays are a competition workout where we focus on cardio and sparring.")
end

if AboutText.find_by(section: "testing_1").nil?
  AboutText.create(section: "testing_1",
                   text: "At the end of each semester the club hosts a belt testing.  The testing is held during one of the regular practice times, and the date will be announced during the semester.")
end

if AboutText.find_by(section: "testing_2").nil?
  AboutText.create(section: "testing_2",
                   text: "Students should print out the appropriate testing sheet from below. The sheet should be filled out and brought on the day of testing.")
end

images = ["about_1", "about_2", "about_3", "about_4", "full_1", "full_2",
          "full_3", "full_4", "testing_1"]
images.each do |name|
  if Image.find_by(name: name).nil?
    Image.create(name: name)
  end
end

links = ["donate", "dues_regular", "dues_traveling", "volunteer", "join"]
links = [
  {name: "donate", path: "donate"},
  {name: "dues_regular", path: "dues/non-traveling"},
  {name: "dues_traveling", path: "dues/traveling"},
  {name: "volunteer", path: "volunteer"},
  {name: "join", path: "join"},
]
links.each do |info|
  l = Link.find_by(name: info[:name])
  if l.nil?
    Link.create(name: info[:name], path: info[:path], url: "https://uttkd.org")
  else
    l.update_attribute(:path, info[:path])
  end
end

sheets = [
  {name: "10th-9th-gup", title: "White Belt - Yellow Stripe"},
  {name: "9th-8th-gup",  title: "Yellow Stripe - Yellow Belt"},
  {name: "8th-7th-gup",  title: "Yellow Belt - Green Stripe"},
  {name: "7th-6th-gup",  title: "Green Stripe - Green Belt"},
  {name: "6th-5th-gup",  title: "Green Belt - Blue Stripe"},
  {name: "5th-4th-gup",  title: "Blue Stripe - Blue Belt"},
  {name: "4th-3rd-gup",  title: "Blue Belt - Red Stripe"},
  {name: "3rd-2nd-gup",  title: "Red Stripe - Red Belt"},
  {name: "2nd-1st-gup",  title: "Red Belt - Black Stripe"},
  {name: "1st-1st-dan",  title: "Black Stripe - 1st Dan"},
  {name: "1st-2nd-dan",  title: "1st Dan - 2nd Dan"},
  {name: "2nd-3rd-dan",  title: "2nd Dan - 3rd Dan"},
  {name: "3rd-4th-dan",  title: "3rd Dan - 4th Dan"},
  {name: "4th-5th-dan",  title: "4th Dan - 5th Dan"}
]
sheets.each_with_index do |info, index|
  if TestingSheet.find_by(name: info[:name]).nil?
    TestingSheet.create(name: info[:name], title: info[:title], order: index)
  end
end
