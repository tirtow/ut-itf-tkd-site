class AboutTextsControllerTest < ActionDispatch::IntegrationTest

  test "visitor should not be able to route to edit" do
    get text_edit_path(section: "one")
    assert_redirected_to(root_url)
  end

  test "user should be able to route to edit" do
    log_in_as(users(:user))
    get text_edit_path(section: "one")
    assert_response(:success)
  end

  test "should not be able to route to edit an about text that does not exist" do
    get text_edit_path(section: "invalid")
    assert_redirected_to(root_url)
  end

  test "visitor cannot update an about text" do
    patch text_update_path(section: "one", about_text: {text: "foo"})
    assert(AboutText.find_by(section: "one").text != "foo")
    assert_redirected_to(root_url)
  end

  test "user can update an about text" do
    log_in_as(users(:user))
    patch text_update_path(section: "one", about_text: {text: "foo"})
    assert(AboutText.find_by(section: "one").text == "foo")
    assert_redirected_to(root_url)
  end

  test "should not be able to update an about text that does not exist" do
    log_in_as(users(:user))
    patch text_update_path(section: "invalid", about_text: {text: "foo"})
    assert_nil(AboutText.find_by(section: "invalid"))
    assert_redirected_to(root_url)
  end

  test "should not be able to make text empty" do
    log_in_as(users(:user))
    patch text_update_path(section: "one", about_text: {text: ""})
    assert(AboutText.find_by(section: "one").text != "")
  end

  test "visitor cannot route to index" do
    get texts_path
    assert_redirected_to(root_url)
  end

  test "user can route to index" do
    log_in_as(users(:user))
    get texts_path
    assert_response(:success)
  end
end
