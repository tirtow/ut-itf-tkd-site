class AccountActivationsControllerTest < ActionDispatch::IntegrationTest

  test "account should be activated if valid" do
    get edit_account_activation_url(id: "foobar",
                                    email: "unactivated@example.com")
    assert(User.find_by(email: "unactivated@example.com").activated?)
    assert_redirected_to(profile_password_set_path)
  end

  test "activation should fail if invalid link" do
    get edit_account_activation_url(id: "invalid",
                                    email: "unactivated@example.com")
    assert_not(User.find_by(email: "unactivated@example.com").activated?)
    assert_redirected_to(root_url)
  end

  test "activation should fail if account already activated" do
    get edit_account_activation_url(id: "foobar",
                                    email: "other@example.com")
    assert(User.find_by(email: "other@example.com").activated?)
    assert_redirected_to(root_url)
  end

  test "activation should fail if email is invalid" do
    get edit_account_activation_url(id: "foobar",
                                    email: "invalid@example.com")
    assert_redirected_to(root_url)
  end
end
