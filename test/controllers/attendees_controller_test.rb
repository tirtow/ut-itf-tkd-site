class AttendeesControllerTest < ActionDispatch::IntegrationTest

  test "visitor can route to new" do
    get attendees_new_path(id: "open")
    assert_response(:success)
  end

  test "user can route to new" do
    log_in_as(users(:user))
    get attendees_new_path(id: "open")
    assert_response(:success)
  end

  test "cannot route to new for a tournament that does not exist" do
    get attendees_new_path(id: "invalid")
    assert_redirected_to(tournaments_path)
  end

  test "cannot route to new for a tournament that is not open" do
    get attendees_new_path(id: "closed")
    assert_redirected_to(tournaments_path)
  end

  test "visitor can register for a tournament" do
    post attendees_create_path(id: "open", attendee: {
      first_name:      "John",
      last_name:       "Doe",
      rank:            "0",
      school:          "Test School",
      instructor:      "Test Instructor",
      patterns:        "1",
      sparring:        "1",
      breaking:        "1",
      team_demo:       "0",
      age:             "10",
      weight_class:    "0",
      sex:             "0",
      street:          "foo",
      city:            "foo",
      state:           "Texas",
      zip:             "12345",
      phone:           "123-456-7890",
      email:           "john.doe@example.com",
      accepted_waiver: "1",
      tournament_id:   "1"
    })
    assert_not_nil(Attendee.find_by(first_name: "John", last_name: "Doe"))
    assert_redirected_to(attendees_confirmation_path(id: "open"))
  end

  test "user can register for a tournament" do
    log_in_as(users(:user))
    post attendees_create_path(id: "open", attendee: {
      first_name:      "John",
      last_name:       "Doe",
      rank:            "0",
      school:          "Test School",
      instructor:      "Test Instructor",
      patterns:        "1",
      sparring:        "1",
      breaking:        "1",
      team_demo:       "0",
      age:             "10",
      weight_class:    "0",
      sex:             "0",
      street:          "foo",
      city:            "foo",
      state:           "Texas",
      zip:             "12345",
      phone:           "123-456-7890",
      email:           "john.doe@example.com",
      accepted_waiver: "1",
      tournament_id:   "1"
    })
    assert_not_nil(Attendee.find_by(first_name: "John", last_name: "Doe"))
    assert_redirected_to(attendees_confirmation_path(id: "open"))
  end

  test "should fail to register if did not accept waiver" do
    post attendees_create_path(id: "open", attendee: {
      first_name:      "John",
      last_name:       "Doe",
      rank:            "0",
      school:          "Test School",
      instructor:      "Test Instructor",
      patterns:        "1",
      sparring:        "1",
      breaking:        "1",
      team_demo:       "0",
      age:             "10",
      weight_class:    "0",
      sex:             "0",
      street:          "foo",
      city:            "foo",
      state:           "Texas",
      zip:             "12345",
      phone:           "123-456-7890",
      email:           "john.doe@example.com",
      tournament_id:   "1"
    })
    assert_nil(Attendee.find_by(first_name: "John", last_name: "Doe"))
  end

  test "should not be able to register for tournament that is not open" do
    post attendees_create_path(id: "closed", attendee: {
      first_name:      "John",
      last_name:       "Doe",
      rank:            "0",
      school:          "Test School",
      instructor:      "Test Instructor",
      patterns:        "1",
      sparring:        "1",
      breaking:        "1",
      team_demo:       "0",
      age:             "10",
      weight_class:    "0",
      sex:             "0",
      street:          "foo",
      city:            "foo",
      state:           "Texas",
      zip:             "12345",
      phone:           "123-456-7890",
      email:           "john.doe@example.com",
      accepted_waiver: "1",
      tournament_id:   "2"
    })
    assert_nil(Attendee.find_by(first_name: "John", last_name: "Doe"))
    assert_redirected_to(tournaments_path)
  end

  test "visitor cannot route to show" do
    get attendee_path(id: "closed", attendee_id: 1)
    assert_redirected_to(root_url)
  end

  test "user can route to show" do
    log_in_as(users(:user))
    get attendee_path(id: "closed", attendee_id: 1)
    assert_response(:success)
  end

  test "cannot show an attendee that does not exist" do
    log_in_as(users(:user))
    get attendee_path(id: "closed", attendee_id: -1)
    assert_redirected_to(tournament_path(id: "closed"))
  end

  test "visitor cannot checkin an attendee" do
    patch attendees_checkin_path(id: "closed", attendee_id: 1)
    assert_redirected_to(root_url)
    assert_not(Attendee.find(1).checked_in?)
  end

  test "user can checkin an attendee" do
    log_in_as(users(:user))
    patch attendees_checkin_path(id: "closed", attendee_id: 1)
    assert_redirected_to(tournaments_checkin_path(id: "closed"))
    assert(Attendee.find(1).checked_in?)
  end

  test "cannot checkin an attendee that does not exist" do
    log_in_as(users(:user))
    patch attendees_checkin_path(id: "closed", attendee_id: -1)
    assert_redirected_to(tournament_path(id: "closed"))
  end

  test "visitor cannot uncheckin an attendee" do
    patch attendees_uncheckin_path(id: "closed", attendee_id: 2)
    assert_redirected_to(root_url)
    assert(Attendee.find(2).checked_in?)
  end

  test "user can uncheckin an attendee" do
    log_in_as(users(:user))
    patch attendees_uncheckin_path(id: "closed", attendee_id: 2)
    assert_redirected_to(tournaments_checkin_path(id: "closed"))
    assert_not(Attendee.find(2).checked_in?)
  end

  test "cannot uncheckin an attendee that does not exist" do
    log_in_as(users(:user))
    patch attendees_uncheckin_path(id: "closed", attendee_id: -1)
    assert_redirected_to(tournament_path(id: "closed"))
  end

  test "visitor should be able to route to confirm" do
    get attendees_confirmation_path(id: "open")
    assert_response(:success)
  end

  test "user should be able to route to confirm" do
    log_in_as(users(:user))
    get attendees_confirmation_path(id: "open")
    assert_response(:success)
  end

  test "should not be able to route to confirmation for tournament that does not exist" do
    get attendees_confirmation_path(id: "invalid")
    assert_redirected_to(tournaments_path)
  end

  test "visitor cannot mark an attendee as paid" do
    patch attendees_paid_path(id: "closed", attendee_id: 1)
    assert_redirected_to(root_url)
    assert_not(Attendee.find(1).paid?)
  end

  test "user can mark an attendee as paid" do
    log_in_as(users(:user))
    patch attendees_paid_path(id: "closed", attendee_id: 1)
    assert_redirected_to(tournaments_checkin_path(id: "closed"))
    assert(Attendee.find(1).paid?)
  end

  test "cannot mark an attendee as paid that does not exists" do
    log_in_as(users(:user))
    patch attendees_paid_path(id: "closed", attendee_id: -1)
    assert_redirected_to(tournament_path(id: "closed"))
  end

  test "visitor cannot unmark an attendee as paid" do
    patch attendees_unpaid_path(id: "closed", attendee_id: 2)
    assert_redirected_to(root_url)
    assert(Attendee.find(2).paid?)
  end

  test "user can unmark an attendee as paid" do
    log_in_as(users(:user))
    patch attendees_unpaid_path(id: "closed", attendee_id: 2)
    assert_redirected_to(tournaments_checkin_path(id: "closed"))
    assert_not(Attendee.find(2).paid?)
  end

  test "cannot unmark an attendee as paid that does not exist" do
    log_in_as(users(:user))
    patch attendees_unpaid_path(id: "closed", attendee_id: -1)
    assert_redirected_to(tournament_path(id: "closed"))
  end
end
