class ImagesControllerTest < ActionDispatch::IntegrationTest

  test "visitor cannot route to index" do
    get images_path
    assert_redirected_to(root_url)
  end

  test "user can route to index" do
    log_in_as(users(:user))
    get images_path
    assert_response(:success)
  end

  test "visitor cannot route to edit" do
    get images_edit_path(id: "test_1")
    assert_redirected_to(root_url)
  end

  test "user can route to edit" do
    log_in_as(users(:user))
    get images_edit_path(id: "test_1")
    assert_response(:success)
  end

  test "cannot route to edit image that does not exist" do
    log_in_as(users(:user))
    get images_edit_path(id: "invalid")
    assert_redirected_to(images_path)
  end

  test "visitor cannot update an image" do
    patch images_update_path(id: "test_1", image: {
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_redirected_to(root_url)
  end
end
