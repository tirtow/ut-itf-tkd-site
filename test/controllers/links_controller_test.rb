class LinksControllerTest < ActionDispatch::IntegrationTest

  test "visitor should not be able to route to index" do
    get links_path
    assert_redirected_to(root_url)
  end

  test "user can route to index" do
    log_in_as(users(:user))
    get links_path
    assert_response(:success)
  end

  test "visitor cannot route to edit a link" do
    get link_edit_path(name: "donate")
    assert_redirected_to(root_url)
  end

  test "user can route to edit a link" do
    log_in_as(users(:user))
    get link_edit_path(name: "donate")
    assert_response(:success)
  end

  test "cannot route to edit a link that does not exist" do
    log_in_as(users(:user))
    get link_edit_path(name: "invalid")
    assert_redirected_to(links_path)
  end

  test "visitor cannot update a link" do
    patch link_update_path(name: "donate",
                           link: {url: "https://example.com/test"})
    assert(Link.get("donate").url != "https://example.com/test")
    assert_redirected_to(root_url)
  end

  test "user can update a link" do
    log_in_as(users(:user))
    patch link_update_path(name: "donate",
                           link: {url: "https://example.com/test"})
    assert(Link.get("donate").url == "https://example.com/test")
    assert_redirected_to(links_path)
  end

  test "should fail to update if link is invalid" do
    log_in_as(users(:user))
    patch link_update_path(name: "donate",
                           link: {url: "invalid"})
    assert(Link.get("donate").url == "https://example.com")
  end

  test "visitor should be able to route to donate redirect" do
    get donate_path
    assert_redirected_to(Link.get("donate").url)
  end

  test "visitor should be able to route to dues_traveling redirect" do
    get dues_traveling_path
    assert_redirected_to(Link.get("dues_traveling").url)
  end

  test "visitor should be able to route to volunteer redirect" do
    get volunteer_path
    assert_redirected_to(Link.get("volunteer").url)
  end

  test "visitor should be able to route to join redirect" do
    get join_path
    assert_redirected_to(Link.get("join").url)
  end

  test "should fail gracefully if cannot find link" do
    Link.get("donate").delete
    get donate_path
    assert_redirected_to(root_url)
  end
end
