class PracticeTimesControllerTest < ActionDispatch::IntegrationTest

  test "visitor cannot route to create new practice time" do
    get practices_new_path
    assert_redirected_to(root_url)
  end

  test "user can route to create new practice time" do
    log_in_as(users(:user))
    get practices_new_path
    assert_response(:success)
  end

  test "visitor cannot create new practice time" do
    post practices_new_path(practice_time: {
      day: 0,
      time: "foo",
      location: "bar"
    })
    assert_nil(PracticeTime.find_by(time: "foo"))
    assert_redirected_to(root_url)
  end

  test "user can create new practice time" do
    log_in_as(users(:user))
    post practices_new_path(practice_time: {
      day: 0,
      time: "foo",
      location: "bar"
    })
    assert_not_nil(PracticeTime.find_by(time: "foo"))
    assert_redirected_to(practices_url)
  end

  test "create should fail if missing field" do
    log_in_as(users(:user))
    post practices_new_path(practice_time: {
      time: "foo",
      location: "bar"
    })
    assert_nil(PracticeTime.find_by(time: "foo"))
  end

  test "visitor cannot route to index" do
    get practices_path
    assert_redirected_to(root_url)
  end

  test "user can route to index" do
    log_in_as(users(:user))
    get practices_path
    assert_response(:success)
  end

  test "visitor cannot delete a practice time" do
    delete practices_delete_path(id: 1)
    assert_not_nil(PracticeTime.find_by(id: 1))
    assert_redirected_to(root_url)
  end

  test "user can delete a practice time" do
    log_in_as(users(:user))
    delete practices_delete_path(id: 1)
    assert_nil(PracticeTime.find_by(id: 1))
    assert_redirected_to(practices_url)
  end

  test "should not be able to delete a practice time that does not exist" do
    log_in_as(users(:user))
    delete practices_delete_path(id: -1)
    assert_nil(PracticeTime.find_by(id: -1))
    assert_redirected_to(practices_url)
  end
end
