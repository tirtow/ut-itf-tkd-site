class RepresentativesControllerTest < ActionDispatch::IntegrationTest

  test "visitor cannot route to new" do
    get representatives_new_path
    assert_redirected_to(root_url)
  end

  test "user can route to new" do
    log_in_as(users(:user))
    get representatives_new_path
    assert_response(:success)
  end

  test "visitor cannot create a new rep" do
    post representatives_new_path(representative: {
      name: "John Doe",
      email: "john.doe@example.com",
      position: Representative::PRESIDENT,
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_nil(Representative.find_by(name: "John Doe"))
    assert_redirected_to(root_url)
  end

  test "user can create a new rep" do
    log_in_as(users(:user))
    post representatives_new_path(representative: {
      name: "John Doe",
      email: "john.doe@example.com",
      position: Representative::PRESIDENT,
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_not_nil(Representative.find_by(name: "John Doe"))
    assert_redirected_to(representatives_path)
  end

  test "create should fail if missing a field" do
    log_in_as(users(:user))
    post representatives_new_path(representative: {
      name: "John Doe",
      position: Representative::PRESIDENT,
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_nil(Representative.find_by(name: "John Doe"))
  end

  test "should not be able to create multiple presidents" do
    log_in_as(users(:user))
    post representatives_new_path(representative: {
      name: "John Doe",
      email: "john.doe@example.com",
      position: Representative::PRESIDENT,
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_not_nil(Representative.find_by(name: "John Doe"))

    post representatives_new_path(representative: {
      name: "John Smith",
      email: "john.smith@example.com",
      position: Representative::PRESIDENT,
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_nil(Representative.find_by(name: "John Smith"))
  end

  test "should not be able to create multiple vice-presidents" do
    log_in_as(users(:user))
    post representatives_new_path(representative: {
      name: "John Smith",
      email: "john.smith@example.com",
      position: Representative::VP,
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_nil(Representative.find_by(name: "John Smith"))
  end

  test "should be able to create multiple representatives" do
    log_in_as(users(:user))
    post representatives_new_path(representative: {
      name: "John Doe",
      email: "john.doe@example.com",
      position: Representative::REP,
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_not_nil(Representative.find_by(name: "John Doe"))

    post representatives_new_path(representative: {
      name: "John Smith",
      email: "john.smith@example.com",
      position: Representative::REP,
      photo: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/representatives/michael_t.jpg"),
        "image/jpg")
    })
    assert_not_nil(Representative.find_by(name: "John Smith"))
  end

  test "visitor should not be able to delete a rep" do
    delete representatives_delete_path(id: 1)
    assert_not_nil(Representative.find_by(id: 1))
    assert_redirected_to(root_url)
  end

  test "user should be able to delete a rep" do
    log_in_as(users(:user))
    delete representatives_delete_path(id: 1)
    assert_nil(Representative.find_by(id: 1))
    assert_redirected_to(representatives_path)
  end

  test "visitor should not be able to route to edit a rep" do
    get representatives_edit_path(id: 1)
    assert_redirected_to(root_url)
  end

  test "user should be able to route to edit a rep" do
    log_in_as(users(:user))
    get representatives_edit_path(id: 1)
    assert_response(:success)
  end

  test "visitor should not be able to update a rep" do
    patch representatives_update_path(id: 1, representative: {name: "new"})
    assert(Representative.find(1).name != "new")
    assert_redirected_to(root_url)
  end

  test "user should be able to update a rep" do
    log_in_as(users(:user))
    patch representatives_update_path(id: 1, representative: {name: "new"})
    assert(Representative.find(1).name == "new")
    assert_redirected_to(representatives_path)
  end

  test "update should fail if field is invalid" do
    log_in_as(users(:user))
    patch representatives_update_path(id: 1, representative: {
      name: "new",
      email: "invalid"
    })
    assert(Representative.find(1).name != "new")
  end

  test "visitor should not be able to route to index" do
    get representatives_path
    assert_redirected_to(root_url)
  end

  test "user should be able to route to index" do
    log_in_as(users(:user))
    get representatives_path
    assert_response(:success)
  end

end
