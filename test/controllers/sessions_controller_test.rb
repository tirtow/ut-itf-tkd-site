class SessionsControllerTest < ActionDispatch::IntegrationTest

  test "visitor can route to login" do
    get login_path
    assert_response(:success)
  end

  test "user cannot route to login" do
    log_in_as(users(:user))
    get login_path
    assert_redirected_to(root_url)
  end

  test "visitor can login" do
    post login_path(session: {email: "user@example.com", password: "foobar123"})
    assert(current_user.id == 1)
    assert_redirected_to(root_url)
  end

  test "user cannot login" do
    log_in_as(users(:user))
    post login_path(session: {email: "other@example.com",
                              password: "foobar123"})
    assert(current_user.id == 1)
    assert_redirected_to(root_url)
  end

  test "should not login if have wrong password" do
    post login_path(session: {email: "user@example.com", password: "foobar"})
    assert_nil(current_user)
  end

  test "should not login if account is not activated" do
    post login_path(session: {email: "unactivated@example.com",
                              password: "foobar123"})
    assert_nil(current_user)
  end

  test "user can logout" do
    log_in_as(users(:user))
    delete logout_path
    assert_nil(current_user)
    assert_redirected_to(root_url)
  end

end
