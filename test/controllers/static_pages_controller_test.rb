class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should be able to route to home" do
    get root_url
    assert_response(:success)
  end

  test "should be able to route to dues" do
    get dues_url
    assert_response(:success)
  end
end
