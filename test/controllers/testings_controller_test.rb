class TestingsControllerTest < ActionDispatch::IntegrationTest
  test "visitor cannot route to index" do
    get testing_sheets_edit_path
    assert_redirected_to(root_url)
  end

  test "user can route to index" do
    log_in_as(users(:user))
    get testing_sheets_edit_path
    assert_response(:success)
  end

  test "visitor cannot route to edit" do
    get testing_edit_path(id: "one")
  end

  test "user can route to edit" do
    log_in_as(users(:user))
    get testing_edit_path(id: "one")
    assert_response(:success)
  end

  test "should redirect if route to edit invalid testing sheet" do
    log_in_as(users(:user))
    get testing_edit_path(id: "invalid")
    assert_redirected_to(testing_sheets_edit_path)
  end

  test "visitor cannot update a testing sheet" do
    patch testing_update_path(id: "one", testing_sheet: {
      file: fixture_file_upload(
        Rails.root.join("test/fixtures/files/uploads/testing_sheets/sheet.pdf"),
        "application/pdf")
    })
    assert_redirected_to(root_url)
  end
end
