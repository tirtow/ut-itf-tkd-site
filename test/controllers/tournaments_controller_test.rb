class TournamentsControllerTest < ActionDispatch::IntegrationTest

  test "visitor should not be able to route to create new tournament" do
    get tournaments_new_path
    assert_redirected_to(root_url)
  end

  test "user should be able to route to create new tournament" do
    log_in_as(users(:user))
    get tournaments_new_path
    assert_response(:success)
  end

  test "visitor should not be able to create a new tournament" do
    post tournaments_new_path(tournament: {
      name: "foo",
      event_date: DateTime.now,
      path: "valid-path",
      payment_link: "https://example.com",
      checkin_time: "8:00 am",
      start_time: "9:30 am"
    })
    assert_nil(Tournament.find_by(name: "foo"))
    assert_redirected_to(root_url)
  end

  test "user should be able to create a new tournament" do
    log_in_as(users(:user))
    post tournaments_new_path(tournament: {
      name: "foo",
      event_date: DateTime.now,
      path: "valid-path",
      payment_link: "https://example.com",
      checkin_time: "8:00 am",
      start_time: "9:30 am"
    })
    assert_not_nil(Tournament.find_by(name: "foo"))
    assert_redirected_to(tournaments_path)
  end

  test "should fail to create if has invalid path" do
    log_in_as(users(:user))
    post tournaments_new_path(tournament: {
      name: "foo",
      event_date: DateTime.now,
      path: "invalid path",
      payment_link: "https://example.com",
      checkin_time: "8:00 am",
      start_time: "9:30 am"
    })
    assert_nil(Tournament.find_by(name: "foo"))
  end

  test "should fail to create if missing fields" do
    log_in_as(users(:user))
    post tournaments_new_path(tournament: {
      name: "foo",
    })
    assert_nil(Tournament.find_by(name: "foo"))
  end

  test "visitor should be able to route to index" do
    get tournaments_path
    assert_response(:success)
  end

  test "user should be able to route to index" do
    log_in_as(users(:user))
    get tournaments_path
    assert_response(:success)
  end

  test "visitor should not be able to route to manage" do
    get tournaments_manage_path(id: "open")
    assert_redirected_to(root_url)
  end

  test "user should be able to route to manage" do
    log_in_as(users(:user))
    get tournaments_manage_path(id: "open")
    assert_response(:success)
  end

  test "visitor should be able to route to show" do
    get tournament_path(id: "open")
    assert_response(:success)
  end

  test "user should be able to route to show" do
    log_in_as(users(:user))
    get tournament_path(id: "open")
    assert_response(:success)
  end

  test "visitor should not be able to open registration" do
    patch tournaments_open_path(id: "closed")
    assert_not(Tournament.find(2).registration_open?)
    assert_redirected_to(root_url)
  end

  test "user should be able to open registration" do
    log_in_as(users(:user))
    patch tournaments_open_path(id: "closed")
    assert(Tournament.find(2).registration_open?)
    assert_redirected_to(tournaments_manage_path(id: "closed"))
  end

  test "should not be able to open registration for a tournament that does not exist" do
    log_in_as(users(:user))
    patch tournaments_open_path(id: "invalid")
    assert_redirected_to(tournaments_path)
  end

  test "visitor should not be able to close registration" do
    patch tournaments_close_path(id: "open")
    assert(Tournament.find(1).registration_open?)
    assert_redirected_to(root_url)
  end

  test "user should be able to close registration" do
    log_in_as(users(:user))
    patch tournaments_close_path(id: "open")
    assert_not(Tournament.find(1).registration_open?)
    assert_redirected_to(tournaments_manage_path(id: "open"))
  end

  test "should not be able to close registration for a tournament that does not exist" do
    log_in_as(users(:user))
    patch tournaments_close_path(id: "invalid")
    assert_redirected_to(tournaments_path)
  end

  test "visitor should not be able to route to checkin" do
    get tournaments_checkin_path(id: "open")
    assert_redirected_to(root_url)
  end

  test "user should be able to route to checkin" do
    log_in_as(users(:user))
    get tournaments_checkin_path(id: "open")
    assert_response(:success)
  end

  test "should not be able to route to checkin for tournament that does not exist" do
    log_in_as(users(:user))
    get tournaments_checkin_path(id: "invalid")
    assert_redirected_to(tournaments_path)
  end

  test "visitor should not be able to route to edit a tournament" do
    get tournaments_edit_path(id: "open")
    assert_redirected_to(root_url)
  end

  test "user should be able to route to edit a tournament" do
    log_in_as(users(:user))
    get tournaments_edit_path(id: "open")
    assert_response(:success)
  end

  test "should not be able to route to edit a tournament that does not exist" do
    log_in_as(users(:user))
    get tournaments_edit_path(id: "invalid")
    assert_redirected_to(tournaments_path)
  end

  test "visitor should not be able to update a tournament" do
    patch tournaments_update_path(id: "open", tournament: {name: "different"})
    assert(Tournament.find(1).name != "different")
    assert_redirected_to(root_url)
  end

  test "user can update a tournament" do
    log_in_as(users(:user))
    patch tournaments_update_path(id: "open", tournament: {name: "different"})
    assert(Tournament.find(1).name == "different")
    assert_redirected_to(tournaments_manage_path(id: "open"))
  end

  test "cannot update a tournament that does not exist" do
    log_in_as(users(:user))
    patch tournaments_update_path(id: "invalid", tournament: {name: "doesnotexist"})
    assert_nil(Tournament.find_by(name: "doesnotexist"))
    assert_redirected_to(tournaments_path)
  end

  test "should fail to update if a field is invalid" do
    log_in_as(users(:user))
    patch tournaments_update_path(id: "open", tournament: {name: ""})
    assert(Tournament.find(1).name != "")
  end

  test "visitor cannot route to generate csv" do
    get tournaments_csv_path(id: "open")
    assert_redirected_to(root_url)
  end

  test "user can route to generate csv" do
    log_in_as(users(:user))
    get tournaments_csv_path(id: "open")
    assert_response(:success)
  end

  test "visitor cannot route to filter checkin" do
    get tournaments_checkin_filter_path(id: "open", query: "test")
    assert_redirected_to(root_url)
  end

  test "should redirect if invalid tournament for checkin filter" do
    log_in_as(users(:user))
    get tournaments_checkin_filter_path(id: "invalid", query: "test")
    assert_redirected_to(tournaments_path)
  end

  test "user should be able to route to filter checkin" do
    begin
      log_in_as(users(:user))
      get tournaments_checkin_filter_path(id: "open", query: "test")
    rescue => e
      assert(e.to_s == "ActionController::UnknownFormat")
    end
  end

  test "visitor cannot route to view attendees" do
    get tournaments_attendees_show_path(id: "open")
    assert_redirected_to(root_url)
  end

  test "user can route to view attendees" do
    log_in_as(users(:user))
    get tournaments_attendees_show_path(id: "open")
    assert_response(:success)
  end

  test "visitor cannot route to filter attendees" do
    get tournaments_attendees_filter_path(id: "open")
    assert_redirected_to(root_url)
  end

  test "user can route to filter attendees" do
    begin
      log_in_as(users(:user))
      get tournaments_attendees_filter_path(id: "open")
    rescue => e
      assert(e.to_s == "ActionController::UnknownFormat")
    end
  end
end
