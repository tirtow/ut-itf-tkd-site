class TransactionsControllerTest < ActionDispatch::IntegrationTest
  test "visitor cannot route to home" do
    get transactions_path
    assert_redirected_to(root_url)
  end

  test "user can route to home" do
    log_in_as(users(:user))
    get transactions_path
    assert_response(:success)
  end

  test "visitor cannot route to update" do
    get transactions_update_path
    assert_redirected_to(root_url)
  end

  test "user can route to update" do
    log_in_as(users(:user))
    get transactions_update_path
    assert_response(:success)
  end

  test "visitor cannot route to revenue charts" do
    get charts_revenue_path
    assert_redirected_to(root_url)
  end

  test "user can route to revenue charts" do
    log_in_as(users(:user))
    get charts_revenue_path
    assert_response(:success)
  end

  test "visitor cannot route to spending charts" do
    get charts_spending_path
    assert_redirected_to(root_url)
  end

  test "user can route to spending charts" do
    log_in_as(users(:user))
    get charts_spending_path
    assert_response(:success)
  end

  test "visitor cannot route to comparison charts" do
    get charts_comparison_path
    assert_redirected_to(root_url)
  end

  test "user can route to comparison charts" do
    log_in_as(users(:user))
    get charts_comparison_path
    assert_response(:success)
  end
end
