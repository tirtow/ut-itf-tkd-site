class UsersControllerTest < ActionDispatch::IntegrationTest

  test "visitor should not be able to route to change password" do
    get profile_password_change_path
    assert_redirected_to(root_url)
  end

  test "user should be able to route to change password" do
    log_in_as(users(:user))
    get profile_password_change_path
    assert_response(:success)
  end

  test "visitor should not be able to change password" do
    patch profile_password_change_path(user: {
      old_password:          "foobar123",
      new_password:          "foobar111",
      password_confirmation: "foobar111"
    })
    assert_redirected_to(root_url)
  end

  test "user should be able to change password" do
    log_in_as(users(:user))
    patch profile_password_change_path(user: {
      old_password:          "foobar123",
      new_password:          "foobar111",
      password_confirmation: "foobar111"
    })
    assert_redirected_to(root_url)
  end

  test "password change should fail if old password is incorrect" do
    log_in_as(users(:user))
    patch profile_password_change_path(user: {
      old_password:          "incorrect",
      new_password:          "foobar111",
      password_confirmation: "foobar111"
    })
    assert_response(:success)
  end

  test "password change should fail if password does not match" do
    log_in_as(users(:user))
    patch profile_password_change_path(user: {
      old_password:          "foobar123",
      new_password:          "foobar111",
      password_confirmation: "different"
    })
    assert_response(:success)
  end

  test "password change should fail if new password is blank" do
    log_in_as(users(:user))
    patch profile_password_change_path(user: {
      old_password:          "foobar123",
      new_password:          "",
      password_confirmation: ""
    })
    assert_response(:success)
  end

  test "password change should fail if password is same as old password" do
    log_in_as(users(:user))
    patch profile_password_change_path(user: {
      old_password:          "foobar123",
      new_password:          "foobar123",
      password_confirmation: "foobar123"
    })
    assert_response(:success)
  end

  test "visitor cannot route to create new user" do
    get users_new_path
    assert_redirected_to(root_url)
  end

  test "user can route to create new user" do
    log_in_as(users(:user))
    get users_new_path
    assert_response(:success)
  end

  test "visitor cannot create a new user" do
    post users_new_path(user: {email: "test@example.com"})
    assert_nil(User.find_by(email: "test@example.com"))
    assert_redirected_to(root_url)
  end

  test "user can create a new user" do
    log_in_as(users(:user))
    post users_new_path(user: {email: "test@example.com"})
    assert_not_nil(User.find_by(email: "test@example.com"))
    assert_redirected_to(users_path)
  end

  test "should fail to create if email is invalid" do
    log_in_as(users(:user))
    post users_new_path(user: {email: "invalid"})
    assert_nil(User.find_by(email: "invalid"))
  end

  test "visitor cannot route index" do
    get users_path
    assert_redirected_to(root_url)
  end

  test "user can route to index" do
    log_in_as(users(:user))
    get users_path
    assert_response(:success)
  end

  test "visitor cannot delete a user" do
    delete users_delete_path(id: 2)
    assert_not_nil(User.find_by(id: 2))
    assert_redirected_to(root_url)
  end

  test "user can delete a user" do
    log_in_as(users(:user))
    delete users_delete_path(id: 2)
    assert_nil(User.find_by(id: 2))
    assert_redirected_to(users_path)
  end

  test "cannot delete a user that does not exist" do
    log_in_as(users(:user))
    delete users_delete_path(id: -1)
    assert_redirected_to(users_path)
  end

  test "visitor cannot route to set a password" do
    get profile_password_set_path
    assert_redirected_to(root_url)
  end

  test "user without a password can route to set a password" do
    log_in_as(users(:user))
    User.find(1).update_attribute(:password_digest, nil)
    get profile_password_set_path
    assert_response(:success)
  end

  test "user with a password cannot route to set a password" do
    log_in_as(users(:user))
    get profile_password_set_path
    assert_redirected_to(profile_password_change_path)
  end

  test "should redirect user if they have not set a password" do
    log_in_as(users(:user))
    User.find(1).update_attribute(:password_digest, nil)
    get root_url
    assert_redirected_to(profile_password_set_path)
  end

  test "visitor cannot set a password" do
    patch profile_password_set_path(user: {
      password:              "foobar123",
      password_confirmation: "foobar123"
    })
    assert_redirected_to(root_url)
  end

  test "user can set a password" do
    log_in_as(users(:user))
    User.find(1).update_attribute(:password_digest, nil)
    patch profile_password_set_path(user: {
      password:              "foobar123",
      password_confirmation: "foobar123"
    })
    assert(User.find(1).authenticated?("password", "foobar123"))
    assert_redirected_to(root_url)
  end

  test "setting password should fail if passwords do not match" do
    log_in_as(users(:user))
    User.find(1).update_attribute(:password_digest, nil)
    patch profile_password_set_path(user: {
      password:              "foobar123",
      password_confirmation: "different"
    })
    assert_nil(User.find(1).password_digest)
  end

  test "user with a password should not be able to set a password" do
    log_in_as(users(:user))
    patch profile_password_set_path(user: {
      password:              "different",
      password_confirmation: "different"
    })
    assert_not(User.find(1).authenticated?("password", "different"))
    assert_redirected_to(profile_password_change_path)
  end

  test "visitor cannot route to manage" do
    get manage_path
    assert_redirected_to(root_url)
  end

  test "user can route to manage" do
    log_in_as(users(:user))
    get manage_path
    assert_response(:success)
  end
end
