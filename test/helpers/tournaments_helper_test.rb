require 'test_helper'
require 'tournaments_helper.rb'

class TournamentsHelperTest < ActiveSupport::TestCase
  include TournamentsHelper

  test "sort_direction should return asc if name is not sort" do
    assert(sort_direction("alpha", "bravo", "desc") == "asc")
  end

  test "sort_direction should return dir if name is sort" do
    assert(sort_direction("alpha", "alpha", "desc") == "desc")
  end
end
