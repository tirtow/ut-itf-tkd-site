require 'test_helper'

class AttendeeTest < ActiveSupport::TestCase
  test "to_csv should add attributes" do
    expected = [[
      "Joe",
      "Smith",
      "Yellow Stripe",
      "Test School",
      "Test Instructor",
      1,
      "Light Weight",
      "Male",
      true,
      true,
      false,
      false,
      "Test demo team name"
    ]]
    actual = []
    attendees(:one).to_csv(actual)
    assert(expected == actual)
  end
end
