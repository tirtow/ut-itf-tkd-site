require 'test_helper'

class TournamentTest < ActiveSupport::TestCase
  test "sorting of attendees" do
    t = tournaments(:closed)
    expected = [attendees(:one), attendees(:two)]
    assert(expected == t.filter_attendees)
  end

  test "filtering attendees" do
    t = tournaments(:closed)
    expected = [attendees(:two)]
    assert(expected == t.filter_attendees("jr"))
  end

  test "path should be converted to lowercase" do
    Tournament.create(name: "testpath",
                      event_date: DateTime.now,
                      path: "tEsT-pAtH",
                      payment_link: "https://example.com",
                      checkin_time: "8:00 am",
                      start_time: "9:30 am")
    assert(Tournament.find_by(name: "testpath").path == "test-path")
  end

  test "should fail to validate if url is invalid" do
    t = Tournament.new(name: "testpath",
                       event_date: DateTime.now,
                       path: "tEsT-pAtH",
                       payment_link: "example.com",
                       checkin_time: "8:00 am",
                       start_time: "9:30 am")
    assert_not(t.valid?)
  end
end
