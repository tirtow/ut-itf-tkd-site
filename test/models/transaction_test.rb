require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  test "building for html" do
    Transaction.delete_all
	html = '<div class="panel panel-info">' +
		'<div class="panel-heading lead" style="color:black;margin-bottom:0px;">' +
			'<strong>Taekwon-Do ITF - Student Org Account</strong>' +
		'</div>' +
	'</div>' +
			'<tbody>' +
              '<tr><td class="text-center text-middle" style="width:15%;">06/29/19</small></td><td class="text-center text-middle">1</td><td class="text-right text-middle  danger"><strong>-$50</strong></td><td class="text-center text-middle">Payment</td><td class="text-middle">Expenses - Travel</td><td class="text-middle">Flight</td></tr>' +
              '<tr><td class="text-center text-middle" style="width:15%;">09/29/18</small></td><td class="text-center text-middle">2</td><td class="text-right text-middle  danger"><strong>$50</strong></td><td class="text-center text-middle">Deposit</td><td class="text-middle">Income</td><td class="text-middle">Online Dues</td></tr>' +
              '<tr><td class="text-center text-middle" style="width:15%;">02/08/19</small></td><td class="text-center text-middle">3</td><td class="text-right text-middle  danger"><strong>-$50</strong></td><td class="text-center text-middle">Payment</td><td class="text-middle">Expenses - Apparel </td><td class="text-middle">T-Shirts</td></tr>' +
              '<tr><td class="text-center text-middle" style="width:15%;">01/10/19</small></td><td class="text-center text-middle">4</td><td class="text-right text-middle  danger"><strong>--$50</strong></td><td class="text-center text-middle">Payment</td><td class="text-middle">Cancelled</td><td class="text-middle">Medals</td></tr>' +
              '<tr><td class="text-center text-middle" style="width:15%;">10/17/18</small></td><td class="text-center text-middle">4</td><td class="text-right text-middle  danger"><strong>-$50</strong></td><td class="text-center text-middle">Payment</td><td class="text-middle">Expenses - Awards</td><td class="text-middle">Medals</td></tr>' +
            '</tbody>'
    html = Nokogiri::HTML(html)
    Transaction.build_all(html)

    assert_equal(Transaction.count, 3)
    assert(Transaction.find_by(transaction_id: 1).spending?)
    assert(Transaction.find_by(transaction_id: 1).account == "studentorg")
    assert(Transaction.find_by(transaction_id: 2).dues?)
    assert(Transaction.find_by(transaction_id: 3).spending?)
    assert(Transaction.find_by(transaction_id: 4).nil?)
  end
end
