require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "email should be converted to lowercase" do
    u = User.create(email: "FOObar@exAmple.COM", password: "foobar123",
                    password_confirmation: "foobar123")
    assert(u.email == "foobar@example.com")
  end
end
