ENV['RAILS_ENV'] ||= 'test'
require 'simplecov'
SimpleCov.start('rails') do
  add_filter("app/channels/application_cable/channel.rb")
  add_filter("app/channels/application_cable/connection.rb")
  add_filter("app/jobs/application_job.rb")
  add_filter("app/mailers/application_mailer.rb")
  add_filter("app/uploaders/representative_photo_uploader.rb")
  add_filter("image_uploader.rb")
end

require_relative '../config/environment'
require 'rails/test_help'

class CarrierWave::Mount::Mounter
  def store!
    # Not storing uploads in the tests
  end
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  def after_teardown
    super
    CarrierWave.clean_cached_files!(0)
  end
end

class ActionDispatch::IntegrationTest
  include SessionsHelper
  fixtures :all

  def after_teardown
    super
    CarrierWave.clean_cached_files!(0)
  end

  # Logs in a user through the login path
  def log_in_as(user, password: 'foobar123')
    post login_path(params: {session: {email: user.email, password: password}})
  end
end
